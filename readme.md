
# Raw data

- `MACARTH1`: https://nationalmap.gov.au/renewables/ > Wind > Macarthur Wind Farm > Download all avaliable. Gives MW output for each 5min period in CSV format.

https://services.aremi.data61.io/aemo/v6/duidcsv/MACARTH1

# Size of files in directory (in GB)

`ll | cut -c 25- | cut -c -10 | python3 -c 'import sys; l = sys.stdin.readlines()[3:]; print(sum([int(x.strip()) for x in l])/1024**3);'`

Need 266.08 GB for 10 years of download (u, v & wind)


# Storage

In an N320 grid, there are 542080 pts
In 10 years, there are 87648 hrs

To store the u-wind, v-wind and temperature for 10 years with np.float16:
for one pts: takes 0.5MB
for all pts: 265.5 GB
