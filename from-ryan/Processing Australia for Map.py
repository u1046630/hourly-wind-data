import pygrib
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math

grbs = pygrib.open('Australia.grib')
# grbs = pygrib.open('test_aus.grib')

set = set()
Us = {}
Vs = {}

for grb in grbs:
    # print(grb)
    # print(grb.dataDate)
    # print(grb.dataTime)
    # print(grb.name)
    # print(grb.nameECMF)
    # print(grb.julianDay)
    # print(grb.keys())
    # maxt = grb.values
    # print(maxt)
    # lats,lons = grb.latlons()
    # print(lats)
    if grb.year==2015:
        set.add((grb.dataDate,grb.dataTime))
        if grb.name == '100 metre U wind component':
            Us[(grb.dataDate,grb.dataTime)] = grb.values[116][125]
        elif grb.name == '100 metre V wind component':
            Vs[(grb.dataDate,grb.dataTime)] = grb.values[116][125]
    # print(set)
print(set)

count = 0
grbs.seek(0)
grb = grbs.read(1)[0]
lats,lons = grb.latlons()
sum = np.zeros(lats.shape)

print(lats,lons)

speed = {}

for thing in set:
    u = Us[thing]
    v = Vs[thing]
    # sum+=(u*u+v*v)**0.5
    speed[thing] = (u*u+v*v)**0.5
    count+=1
    print(str(count)+"/"+str(len(set)))

out_file = open("wind_farm_data/reanalysis_macarth_2015.csv", "w")

for thing in sorted(speed.keys()):
    out_file.write(str(speed[thing])+"\n")

out_file.close()
#
# # print(sum/count)
# avg = sum/count
#
# # ip.set_cmap('nipy_spectral')
# # cb = plt.colorbar()
#
# # Concatenating colormaps
# top = plt.cm.get_cmap('Blues', 128)
# bottom = plt.cm.get_cmap('jet', 128)
# newcolors = np.vstack((top(np.linspace(0, 1, 128)),
#                        bottom(np.linspace(0, 1, 128))))
# cmap = mpl.colors.ListedColormap(newcolors, name='GreyBlue')
#
# fig, ax = plt.subplots()
# norm = mpl.colors.Normalize(vmin=0, vmax=10)
# im = ax.imshow(avg, norm=norm, cmap='jet', interpolation='none')
# fig.colorbar(im)
# plt.show()
# plt.savefig("temp.png")