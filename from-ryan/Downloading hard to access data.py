import cdsapi
c = cdsapi.Client()
c.retrieve('reanalysis-era5-complete', {
    'class': 'ea',
    'date': '2019-01-01/to/2019-01-31',
    'expver': '1',
    'levelist': '130/131/132/133/134/135/136/137',
    'levtype': 'ml',
    'param': '130/131/132/135',
    'stream': 'oper',
    'time': '00:00:00/01:00:00/02:00:00/03:00:00/04:00:00/05:00:00/06:00:00/07:00:00/08:00:00/09:00:00/10:00:00/11:00:00/12:00:00/13:00:00/14:00:00/15:00:00/16:00:00/17:00:00/18:00:00/19:00:00/20:00:00/21:00:00/22:00:00/23:00:00',
    'type': 'an',
}, 'stuff.grib')

# c = cdsapi.Client()
# import cdsapi
# c.retrieve('reanalysis-era5-complete', {
#     'class': 'ea',
#     'date': '2019-01-01',
#     'expver': '1',
#     'levelist': '137',
#     'levtype': 'ml',
#     'param': '130/131/132/135',
#     'stream': 'enda',
#     'time': '00/to/23/by/1',
#     'type': 'an',
# }, 'output')

# import cdsapi
#
# c = cdsapi.Client()
#
# c.retrieve(
#     'reanalysis-era5-complete',
#     {
#         'product_type': 'reanalysis',
#         'variable': [
#             'temperature', 'u_component_of_wind', 'v_component_of_wind',
#             'vertical_velocity',
#         ],
#         'model_level': '137',
#         'year': '2019',
#         'month': '01',
#         'day': '01',
#         'time': '00:00',
#         'format': 'grib',
#     },
#     'download.grib')
