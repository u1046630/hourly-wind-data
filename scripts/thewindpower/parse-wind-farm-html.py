#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 11:33:51 2020

@author: liam
"""

import pandas as pd, os, progressbar
import glob, html2text

from wind.drives import thewindpower_dir
from funcs_parse import parse_dot_points

input_pattern = thewindpower_dir + 'countries/{}/wind-farm-html/*.html'
output_pattern = thewindpower_dir + 'countries/{}/wind-farm-data.csv'

#############
### funcs ###
#############

def clean_data(datas):
    cleaned = []
    for data in datas:
        temp = {}
        
        for k, v in list(data.items()):
            if k.endswith(' turbines') or k.endswith('1 turbine'):
                temp['Number turbines'] = int(k.split(' ')[0])
                if v == '(manufacturer name not available)':
                    continue
                elif ' (' in v:
                    model = v.split(' (')[0].replace('/', '-')
                    power = int(v.split(' (power ')[1].split(' kW,')[0].replace(' ',''))
                    size = float(v.split('diameter ')[1].split(' m)')[0])
                    temp['Turbine model'] = model
                    temp['Turbine diameter (m)'] = size
                    temp['Turbine power (kW)'] = power
                else:
                    temp['Turbine model'] = v
            elif k == 'Latitude' or k == 'Longitude':
                d,m,s = [float(x[:-1]) for x in v.split(' ')]
                temp[k] = abs(d) + m/60 + s/3600
                if d < 0:
                    temp[k] *= -1
            elif k == 'Hub height':
                temp['Hub height (m)'] = float(v.split(' ')[0])
            elif k == 'Total nominal power':
                temp['Power (kW)'] = int(v.split('kW')[0].replace(',',''))
            elif k in ['Name', 'Part number', 'Developer', 'Operator', 'Commissioning']:
                temp[k] = v
            elif k in ['Operational', 'Planned', 'Under construction', 'Approved']:
                temp['Status'] = k
            elif k.startswith('Dismantled'):
                temp['Status'] = 'Dismantled'
                if '(' in k and ')' in k:
                    temp['Decommissioning'] = k.split('(')[1].split(')')[0]
                
        if 'Onshore wind farm' in data:
            temp['Type'] = 'Onshore'
        elif 'Offshore wind farm':
            temp['Type'] = 'Offshore'
            
        if 'Precise location' in data and data['Precise location'] == 'yes':
            temp['Precise location'] = True
        elif 'Precise localization' in data and data['Precise localization'] == 'yes':
            temp['Precise location'] = True
        else:
            temp['Precise location'] = False
            
        cleaned.append(temp)
    return cleaned

############
### main ###
############

h = html2text.HTML2Text()
h.ignore_links = True

for country in glob.glob(thewindpower_dir + 'countries/*'):
    country = os.path.basename(country)
    if country[0] == '_':
        continue
    print('[*] {}:'.format(country), flush=True)
    output_file = output_pattern.format(country)
    
    results = []

    fnames = glob.glob(input_pattern.format(country))
    for fname in progressbar.progressbar(fnames):
        name = os.path.basename(fname)[:-5]
        
        with open(fname, 'r') as f:
            txt = h.handle(f.read())
            
        lines = [line.strip() for line in txt.split('\n') if len(line.strip()) > 0]
        details, is_parts1 = parse_dot_points(lines, 'Details')
        locations, is_parts2 = parse_dot_points(lines, 'Localisation')
        
        if 'Part #1' not in details:
            details = {'Part #1': details}
        if 'Part #1' not in locations:
            locations = {'Part #1': locations}
        
        for k in details.keys():
            if k in locations:
                details[k].update(locations[k])
            if len(details) > 1:
                details[k]['Name'] = '{} - {}'.format(name, k)
            else:
                details[k]['Name'] = name
            results.append(details[k]) 
            
    cleaned = clean_data(results)
    df = pd.DataFrame(cleaned)
    df = df.set_index('Name')
    df = df.sort_index()
    df.to_csv(output_file)

