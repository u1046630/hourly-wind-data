#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 11:33:51 2020

@author: liam
"""

from lxml import etree
import pandas as pd, glob, os

from wind.drives import thewindpower_dir

input_pattern = thewindpower_dir + 'countries/{}/wind-farm-links.html'
output_pattern = thewindpower_dir + 'countries/{}/wind-farm-links.csv'
table_xpath = '/html/body/div[1]/div[5]/div/table[2]/tbody'
base_url = 'https://www.thewindpower.net/'

htmlparser = etree.HTMLParser()

for country in glob.glob(thewindpower_dir + 'countries/*'):
    country = os.path.basename(country)
    if country[0] == '_':
        continue
    print(country)
    input_file = input_pattern.format(country)
    output_file = output_pattern.format(country)

    with open(input_file, 'r') as f:
        tree = etree.parse(f, htmlparser)
    
    results = []
    rows = tree.xpath("//tr")
    
    for row in rows:
        kids = row.getchildren()
        if len(kids) != 4:
            continue
        cell0,_,_,cell3 = kids
        if cell0.get('class') != 'ligne_tableau1' or cell3.get('class') != 'ligne_tableau1':
            continue
        cell0 = cell0.getchildren()[0]
        if cell0.get('class') != 'lien_standard_tab':
            continue
        results.append({
            'name': cell0.text,
            'url': base_url + cell0.attrib['href'],
            'offshore': cell3.text == 'Yes',
        })
        
    df = pd.DataFrame(results)
    df.to_csv(output_file, index=False)