#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 15:50:21 2020

@author: liam
"""

from lxml import etree
import html2text, glob, progressbar
import ast, pandas as pd

from wind.drives import thewindpower_dir
from funcs_parse import parse_dot_points

input_pattern = thewindpower_dir + 'turbines/html/*.html'
output_dir = thewindpower_dir + 'turbines/power-curve-data/' # for the power curves
output_fname = thewindpower_dir + 'turbines/turbine-data.csv' # for meta data

script_xpath = '/html/body/div[1]/div[5]/div/div/script[2]'

#input_pattern = '/home/liam/Downloads/thewindpower-turbines/*.html'
#output_dir =  '/home/liam/Downloads/thewindpower-turbines/power-curve-data/'
#output_fname = '/home/liam/Downloads/thewindpower-turbines/power-curve-data/meta.csv'

def to_int(s):
    return int(s.split(' ')[0].replace(',',''))

def to_float1(s): # normal
    return float(s.split(' ')[0].replace(',',''))

def to_float2(s): # European style decimal point
    return float(s.split(' ')[0].replace(',','.'))

############
### main ###
############
        
htmlparser = etree.HTMLParser()
results = []
missing = 0

fnames = glob.glob(input_pattern)
for fname in progressbar.progressbar(fnames):
    
    with open(fname, 'r') as f:
        tree = etree.parse(f, htmlparser)
        
    # Get dot point details
    h = html2text.HTML2Text()
    h.ignore_links = True
    with open(fname, 'r') as f:
        txt = h.handle(f.read())
    lines = [line.strip() for line in txt.split('\n') if len(line.strip()) > 0]
    general,_ = parse_dot_points(lines, 'General data')
    tower,_ = parse_dot_points(lines, 'Tower')

    # name of model, example: "Enercon E92-2350"    
    name = general['Manufacturer'].split(' (')[0] + ' ' + general['Model'].replace('/', '-')
    
    results.append({
        'name': name,
        'manufacturer': general['Manufacturer'],
        'model': general['Model'],
        'power': to_int(general['Rated power']),
        'diameter': to_float1(general['Rotor diameter']),
        'power density': to_float1(general['Power density']),
        'min hub height': to_float2(tower.get('Minimum hub height') or tower.get('Hub height') or '0'),
        'max hub height': to_float2(tower.get('Maximum hub height') or tower.get('Hub height') or '0'),
        'class': general.get('Wind class'),
        'power curve': True,
        'html file': fname.split('/')[-1],
    })
    
    # Get power curve (it's inside a script element)
    script = tree.xpath(script_xpath)
    if len(script) == 0:
        missing += 1
        results[-1]['power curve'] = False
        continue
    script = script[0].text
    
    plot_data = script.split("['', 'Power (kW)'],")[1].split(']);')[0].strip()
    plot_data = ast.literal_eval(plot_data) # javascript lists look like pythons ones
    x = [float(i[0]) for i in plot_data]
    y = [float(i[1]) for i in plot_data]
    
    data = pd.DataFrame({'Wind speed (m/s)': x, 'Power (kW)': y})
    data.to_csv(f'{output_dir}{name}.csv', index=False)

print('[*] Done. {} power curves found. {} turbine models are missing power curves.'.format(len(fnames)-missing, missing))

results = pd.DataFrame(results)
results.to_csv(output_fname, index=False)