#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 11:33:51 2020

@author: liam
"""

import pandas as pd
import glob, progressbar, os
from lxml import etree

from wind.drives import thewindpower_dir

input_pattern = thewindpower_dir + 'countries/*/wind-farm-html/*.html'
output_file = thewindpower_dir + 'turbines/turbine-links.csv'

htmlparser = etree.HTMLParser()

results = {}

fnames = glob.glob(input_pattern)
for fname in progressbar.progressbar(fnames):
    name = os.path.basename(fname)[:-5]
    
    with open(fname, 'r') as f:
        tree = etree.parse(f, htmlparser)
        
    for x in tree.xpath('//a'):
        if not('href' in x.attrib and x.attrib['href'].startswith('turbine_en')):
            continue
        manufacturer = x.getparent().getchildren()[0].text
        model = '{} {}'.format(manufacturer, x.text)
        model = model.replace('/','-')
        results[model] = x.attrib['href']
        
models, links = zip(*results.items())
data = pd.DataFrame({'model': models, 'link': links})
data = data.set_index('model')
data = data.sort_index()
data.to_csv(output_file)
