#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 17:09:21 2020

@author: liam
"""

import pandas as pd, os
import urllib.request
import progressbar

from wind.drives import thewindpower_dir

base_url = 'https://www.thewindpower.net/'
input_fname = thewindpower_dir + 'turbines/turbine-links.csv'
output_fname = thewindpower_dir + 'turbines/html/{}.html'

links = pd.read_csv(input_fname)

new_count = 0
for index, row in progressbar.progressbar(list(links.iterrows())):
    model, link = row.model, row.link
    output = output_fname.format(model)
    if os.path.exists(output):
        continue
    #print('[*] {}'.format(model))
    url = base_url + link
    response = urllib.request.urlopen(url)
    assert(response.status == 200)
    with open(output, 'wb') as f:
         f.write(response.read())
    new_count += 1
    
print('[*] done. downloaded {} new turbine models'.format(new_count))