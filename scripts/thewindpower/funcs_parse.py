#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 09:35:44 2020

@author: liam
"""

def parse_dot_points(lines, heading):
    base_dict = {}
    is_parts = False
    current_dict = base_dict
    if '# ' + heading not in lines:
        return {}, False
    i = lines.index('# ' + heading) + 1
    
    while i < len(lines):
        line = lines[i]
        if line.startswith('#### ') and line.endswith('Production forecast'):
            i += 4
        elif line.startswith('### '):
            is_parts = True
            part_name = line[4:]
            if part_name.endswith(':'):
                part_name = part_name[:-1]
            current_dict = {}
            base_dict[part_name] = current_dict
        elif line.startswith('* '):
            c = line.find(': ')
            if c == -1:
                k = line[2:]
                v = True
                current_dict[k] = v
            else:
                k = line[2:c]
                v = line[c+2:]
                current_dict[k] = v
        else:
            break
        i += 1
    
    return base_dict, is_parts

