#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 11:33:51 2020

@author: liam
"""

import pandas as pd, progressbar
import urllib.request, os, glob

from wind.drives import thewindpower_dir

offshore_only = True
input_pattern = thewindpower_dir + 'countries/{}/wind-farm-links.csv'
output_pattern = thewindpower_dir + 'countries/{}/wind-farm-html/'

for country in glob.glob(thewindpower_dir + 'countries/*'):
    country = os.path.basename(country)
    if country[0] == '_':
        continue
    print('[*] {}:'.format(country), flush=True)
    
    input_file = input_pattern.format(country)
    output_dir = output_pattern.format(country)
    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    df = pd.read_csv(input_file)
    
    if offshore_only:
        df = df[df.offshore]
    
    for index, row in progressbar.progressbar(list(df.iterrows())):
        name, url = row['name'], row.url
        output_fname = output_dir + name + '.html'
        if os.path.exists(output_fname):
            continue
        response = urllib.request.urlopen(url)
        assert(response.status == 200)
        with open(output_fname, 'wb') as f:
            f.write(response.read())
        
