#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 12:43:11 2020

@author: liam
"""

import numpy as np
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr

def write_tiff(array, lat, lon, fname):
    # data is np array, indexed by [x,y] (so [lon,lat])

    xmin,ymin,xmax,ymax = [lon.min(),lat.min(),lon.max(),lat.max()]
    ncols,nrows = np.shape(array)
    xres = (xmax-xmin)/(ncols-1)
    yres = (ymax-ymin)/(nrows-1)
    geotransform=(xmin-xres/2,xres,0,ymin-yres/2,0, yres)   
    # That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
    #         top left y, rotation (0 if North is up), n-s pixel resolution)
    # I don't know why rotation is in twice???
    
    output_raster = gdal.GetDriverByName('GTiff').Create(fname, ncols, nrows, 1, gdal.GDT_Float32)  # Open the file
    output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
    srs = osr.SpatialReference()                 # Establish its coordinate encoding
    srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
    output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system 
                                                       # to the file
    output_raster.GetRasterBand(1).WriteArray(array.transpose())   # Writes my array to the raster
    
    output_raster.FlushCache()
    
    
if __name__ == '__main__':

    array = np.array(( (0.1, 0.2, 0.3, 0.4),
                       (0.2, 0.3, 0.4, 0.5),
                       (0.3, 0.4, 0.5, 0.6),
                       (0.4, 0.5, 0.6, 0.7),
                       (0.5, 0.6, 0.7, 0.8) )).transpose()
    # My image array      
    lat = np.array([10.0, 9.5,  9.0,  8.5,  8.0])
    lon = np.array([20.0, 20.5, 21.0, 21.5])
    
    write_tiff(array, lat, lon, 'output.tiff')
    # For each pixel I know it's latitude and longitude.
    # As you'll see below you only really need the coordinates of
    # one corner, and the resolution of the file.
    