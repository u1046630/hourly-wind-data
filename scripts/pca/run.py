#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 09:33:31 2020

@author: liam
"""

import numpy as np, progressbar
from datetime import datetime, timedelta

from wind.access_era5 import get_era5_data
import wind.n320_grid as n320

level = 133
date_start = datetime(2015, 1, 1)
date_end = datetime(2020, 1, 1)
aus_bbox = [-43.6345972634, 113.338953078, -10.6681857235, 153.569469029] # Australia
nsw_bbox = [-41.393294,127.023926,-24.507143,167.475586]

def n320_coords_in_bbox(lat1, lon1, lat2, lon2):
    pts = []
    lon1 %= 360
    lon2 %= 360
    assert(-90 <= lat1 and lat1 <= lat2 and lat2 <= 90)
    assert(0 <= lon1 and lon1 <= lon2 and lon2 <= 360)
    for lat, num_lons in zip(n320.lats, n320.lons):
        if lat >= lat1 and lat < lat2:
            lons = np.linspace(0, 360, num_lons, endpoint=False)
            lons = lons[(lons>=lon1) & (lons<lon2)]
            [pts.append((lat,lon)) for lon in lons]
    return np.array(pts)

coords = n320_coords_in_bbox(*nsw_bbox)
N = coords.shape[0] # number of PCA pts
P = (date_end - date_start) // timedelta(hours=1) # number of PCA dimensions

data = np.empty((N,P), dtype=np.float32)

for n in progressbar.progressbar(range(N)):
    lat,lon = coords[n]
    u_wind = get_era5_data(date_start, date_end, level, 'u', lat, lon, interpolated=False)
    v_wind = get_era5_data(date_start, date_end, level, 'v', lat, lon, interpolated=False)
    data[n,:] = (u_wind**2 + v_wind**2)**0.5
    
mean = np.mean(data, axis=1)
std = np.std(data, axis=1)
for n in range(N):
    data[n] = (data[n] - mean[n]) / std[n]
    
np.save('./wind.npy', data)
np.save('./coords.npy', coords)
