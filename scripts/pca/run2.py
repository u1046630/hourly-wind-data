#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 10:03:50 2020

@author: liam
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.interpolate as interpolate
from sklearn.decomposition import PCA
from sklearn.cluster import AgglomerativeClustering

from write_tiff import write_tiff
        
def select_colour(pts, colours):
    x0, x1 = np.min(pts[:,0]), np.max(pts[:,0])
    y0, y1 = np.min(pts[:,1]), np.max(pts[:,1])
    corners = np.array([(x0,y0),(x0,y1),(x1,y0),(x1,y1)])
    colours = np.array([mpl.colors.to_rgb(c) for c in colours])
    
    n = pts.shape[0]
    outputs = np.empty((n,3))
    outputs[:,0] = interpolate.griddata(corners, colours[:,0], pts) # red
    outputs[:,1] = interpolate.griddata(corners, colours[:,1], pts) # green
    outputs[:,2] = interpolate.griddata(corners, colours[:,2], pts) # blue
    return outputs

def interp_to_grid(coords, colours):
    y0, y1 = np.min(coords[:,0]), np.max(coords[:,0])
    x0, x1 = np.min(coords[:,1]), np.max(coords[:,1])
    res = np.sqrt((x1-x0)*(y1-y0) / len(coords))
    y,x = np.meshgrid(
            np.arange(y0+res/2, y1, res),
            np.arange(x0+res/2, x1, res))
    
    temp = np.array(list(zip(y.flatten(), x.flatten())))
    z = np.empty((*x.shape, 3))
    z[:,:,0] = interpolate.griddata(coords, colours[:,0], temp).reshape(x.shape)
    z[:,:,1] = interpolate.griddata(coords, colours[:,1], temp).reshape(x.shape)
    z[:,:,2] = interpolate.griddata(coords, colours[:,2], temp).reshape(x.shape)
    return x,y,z

print('loading...')
data = np.load('data/wind-nsw-1yr.npy') # shape == (N,P)
data.dtype = np.float32
coords = np.load('data/coords-nsw.npy') # shape == (N,2), coords[0] == (lat,lon)
N,P = data.shape

print('standardising...')
mean = np.mean(data, axis=1)
std = np.std(data, axis=1)
for n in range(N):
    data[n] = (data[n] - mean[n]) / std[n]
    
print('doing pca...')
pca = PCA(n_components=20)
pts = pca.fit_transform(data)[:,:2]

fig = plt.figure(figsize=(15,5))
ax = fig.add_subplot(131)
plt.plot(np.cumsum(pca.explained_variance_ratio_))

clusterer = AgglomerativeClustering(n_clusters=10, affinity = 'euclidean', linkage = 'ward')
clusters = clusterer.fit_predict(pts)

print('selecting colours...')
colours = select_colour(pts[:,:2], ['salmon', 'black', 'white', 'blue'])



print('plotting...')
ax = fig.add_subplot(132)
ax.scatter(pts[:,0], pts[:,1], color=colours, s=0.3)
ax = fig.add_subplot(133)
ax.scatter(coords[:,1], coords[:,0], color=colours)

print('interpolating colours to grid...')
x,y,z = interp_to_grid(coords, colours)
print('saving to raster file...')
write_tiff(z[:,:,0], y, x, 'output1.tiff')
