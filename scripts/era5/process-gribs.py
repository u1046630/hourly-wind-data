#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 10:01:20 2020

@author: liam
"""
# This program does the first half of converting a bunch of spacial-major GRIB
# files to a bunch of temporal-major numpy array files (.npy)

# More detail: 

# GRIB files are slow to access, there is a lot of data packing (compression) and
# it's memory layout is spacial-major (quick to access all the data for one time 
# for whole spacial grid, but slow to access data for one grid point for all time).
# For our purpose we want temporal-major storage, and a file format that allows
# quicker access than GRIB. 

# The first half processes GRIBs in input_dir to .npy files in temp_dir.
# The second half stitches them together into a few larger .npy files and 
# puts the resulting .npy files in output_dir.

# For 10 years of hourly u-wind, v-wind & temperature data, this takes about
# 7hrs for first half, and 2.5hrs for second half.

# input_dir contains GRIB files which contain u-wind, v-wind and temperature data
# on a N320 reduced gaussian grid, for each hour from date_start to date_end.
# assume grib filename is fromdate_todate.grib where fromdate and todate are 
# inclusive and in the format YYYYMMDD.

# temp_dir will contain a bunch of subdirectories (labelled 0, 1, 2, ...)
# each of which will contain .npy arrays of type np.float16 and
# shape (3, num_n320_pts_per_file, n) where n is the number of hours of data 
# contained in each input GRIB file. array[0], array[1] and array[1] represent
# u component of wind, v component temperature respectively.

# output_dir contains numpy arrays of shape (3, num_n320_pts_per_file, N) 
# where N is the number of hours between date_start and date_end. 
# A file name of 100_199.npy means it contains data for N320 grid points 100
# to 199 inclusive.

# There are 542080 grid points, that many individual .npy files is a bit 
# unruly, hence storing a bunch of them together into a few .npz files.
# numpy can then lazy load little bits of it.
# For example num_n320_pts_per_file == 1120, means ending up with 484 .npz
# files, each about 561MB. 

import glob, os, progressbar
import xarray
import numpy as np, pandas as pd
from datetime import datetime, timedelta

from wind.drives import era5_dir

###############
### globals ###
###############

date_start = datetime(1980, 1, 1) # inclusive
date_end = datetime(1990, 1, 1) # exclusive
input_dir = era5_dir + 'level131-1980-1989/rawdata/'
temp_dir = era5_dir + 'level131-1980-1989/npy-temp/'
output_dir = era5_dir + 'level131-1980-1989/data/'

bytes_per_hour = 1086840 * 2 # (2 parameters downloaded)
num_n320_pts = 542080
level_num = 131 # the model level to check against the GRIB file
num_n320_pts_per_file = 1120

do_first_half = True
do_second_half = True

params = {
    'u': 'eastward_wind',
    'v': 'northward_wind',
#    't': 'air_temperature',
}

#############
### funcs ###
#############

def dates_from_fname(fname):
    basename = os.path.basename(fname).split('.')[0]
    date1, date2 = basename.split('_')
    date1 = datetime.strptime(date1, '%Y%m%d')
    date2 = datetime.strptime(date2, '%Y%m%d')
    day_delta = timedelta(days=1)
    num_days = (date2 - date1 + day_delta) // day_delta
    return date1, date2, num_days, basename

def tmp_files(fname, max_days=16):
    date1, date2, total_days, basename = dates_from_fname(fname)
    date2 += timedelta(days=1) # exclusive
    
    if total_days <= 16:
        return [fname] # no need to make tmp files
    
    fifos = []
    with open(fname, 'rb') as f1:
        while date1 < date2:
            num_days = min((date2-date1)//timedelta(days=1), 16)
            path = '/tmp/{}_{}.grib'.format(
                datetime.strftime(date1, '%Y%m%d'),
                datetime.strftime(date1 + timedelta(days=num_days-1), '%Y%m%d'), # inclusive
            )
            with open(path, 'wb') as f2:
                f2.write(f1.read(num_days * 24 * bytes_per_hour))
            fifos.append(path)
            date1 += timedelta(days=num_days)
    return fifos
    
def dataset_checks(data, num_days):
    
    assert(data.dims['time'] == num_days * 24)
    assert(data.dims['values'] == num_n320_pts)
    for k,v in params.items():
        standard_name = getattr(data, k).standard_name
        assert(standard_name == v)
    
    for var in params.keys():
        assert(data[var].GRIB_numberOfPoints == num_n320_pts)
        assert(data[var].GRIB_gridType == 'reduced_gg')
        #assert(data[var].GRIB_typeOfLevel == 'hybrid')
        # try to check for the model level, sometimes it doesn't work
        #lines = [line.strip() for line in str(data[var].hybrid.coords).split('\n')]
        #level = [line.split(' ')[-1] for line in lines if line[:6] == 'hybrid'][0]
        #assert(level == '...' or int(level) == level_num)
        
##################
### first half ###
##################

num_files = num_n320_pts // num_n320_pts_per_file

if do_first_half:
    print('[*] starting first half...', flush=True)

    if os.path.exists(temp_dir):
        # too dangerous to programatically delete large amounts of data
        # make the user do it:
        print('[!] temporary directory already exists ({})'.format(temp_dir), flush=True)
        #raise(Warning)
    else:
        os.mkdir(temp_dir)

    dates = np.arange(date_start, date_end, timedelta(days=1))
    checks = np.zeros(dates.shape, dtype='bool')
    checklist = pd.DataFrame({'date': dates, 'check': checks}).set_index('date')

    fnames = sorted(glob.glob(input_dir + '*.grib'))

    # make sure all the files are there, and they have all the data
    # (if it catches missing data early, it saves a LOT of time)
    # GRIB format is simply a list of messages, so twice as many messages
    # implies exactly twice as many bytes.

    for fname in fnames:
        date1, date2, num_days, basename = dates_from_fname(fname)
        num_bytes = os.stat(fname).st_size
        if num_bytes != num_days * 24 * bytes_per_hour:
            print('[!] file {} is {} bytes, should be {} bytes'.format(
                basename, num_bytes, num_days * 24 * bytes_per_hour
            ), flush=True)
            raise(Warning)
        checklist[(checklist.index >= date1) & (checklist.index <= date2)] = True

    num_missing = (~checklist.check).sum()
    if num_missing > 0:
        print('[!] Warning: data for the following {} days is missing:'.format(num_missing), flush=True)
        for date in checklist[checklist.check == False].index:
            print('      ' + datetime.strftime(date, '%Y-%m-%d'), flush=True)
        raise(Warning)
        
    for i in range(num_files):
        subdir = '{}{}/'.format(temp_dir, i)
        os.mkdir(subdir)
        
    for fname in progressbar.progressbar(fnames):
        print('[*] {}'.format(fname), flush=True)
        
        tmp_fnames = tmp_files(fname)
        for tmp_fname in tmp_fnames:
            
            date1, date2, num_days, basename = dates_from_fname(tmp_fname)
        
            data = xarray.open_dataset(tmp_fname, engine='cfgrib')
            dataset_checks(data, num_days)
            
            for i in range(num_files):
                subdir = '{}{}/'.format(temp_dir, i)
                i_start = i * num_n320_pts_per_file
                i_end = i_start + num_n320_pts_per_file
                u = data.u.values[:, i_start:i_end].transpose()
                v = data.v.values[:, i_start:i_end].transpose()
                #t = data.t.values[:, i_start:i_end].transpose()
                # 'C' for C_CONTINUOUS, it's the memory layout we want
                array = np.array([u,v], order='C', dtype=np.float16)
                np.save(subdir + basename, array)
                
            if tmp_fname.startswith('/tmp'):
                os.remove(tmp_fname)

###################
### second half ###
###################

if do_second_half:
    print('[*] starting second half...', flush=True)
    
    num_hours = (date_end - date_start) // timedelta(hours=1)
    subdirs = glob.glob(temp_dir + '*')
    
    if list(range(num_files)) != sorted([int(os.path.basename(x)) for x in subdirs]):
        print('[!] unexpected subdirectories in {}'.format(temp_dir), flush=True)
        raise(Warning)
        
    if os.path.exists(output_dir):
        # again, make user delete potentially valuable data
        print('[!] output directory already exists ({})'.format(output_dir), flush=True)
        raise(Warning)
    os.mkdir(output_dir)
        
    # we're going to assume all the right files are there, raise error if
    # not enough data when we're done
    for i in progressbar.progressbar(range(num_files)):
        
        fnames = sorted(glob.glob(os.path.join(temp_dir, str(i), '*.npy')))
        arrays = []
        for fname in fnames:
            #print('[*] opening {}'.format(fname), flush=True)
            arrays.append(np.load(fname))
            
        array = np.concatenate(arrays, axis=2)
        
        # make sure result has the shape and memory layout we want
        assert(array.shape == (len(params), num_n320_pts_per_file, num_hours))
        assert(array.flags['C_CONTIGUOUS']) 

        i_start = i * num_n320_pts_per_file # inclusive
        i_end = i_start + num_n320_pts_per_file - 1 # inclusive
        
        array_dict = {}
        for j in range(num_n320_pts_per_file):
            var_name = str(i_start + j) + '_'
            k = 0
            for param_char in params.keys():
                array_dict[var_name + param_char] = array[k,j]
                k += 1
            
        output_fname = os.path.join(output_dir, '{}_{}'.format(i_start, i_end))
        np.savez(output_fname, **array_dict)

print('[*] done!', flush=True)
