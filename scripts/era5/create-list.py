#!/usr/bin/python3

# see what GRIBs have been downloaded, make a list of the ones
# that still need to be downloaded

import glob, os
import numpy as np, pandas as pd
from datetime import datetime, timedelta

#import sys
#sys.path.append('../../python_modules')
from wind.drives import era5_dir

download_dir = era5_dir + 'level131-1980-1989/rawdata/'
list_fname = 'list.txt'
start_date = datetime(2014, 1, 1) # inclusive
end_date = datetime(2020, 1, 1) # exclusive
max_period = timedelta(days=31)

def get_next_month(date):
    y, m = date.year, date.month+1
    if m == 13:
        m = 1
        y += 1
    return datetime(y, m, 1)
    
def get_next_downloaded(df, date):
    found = df[df.index > date][df.downloaded]
    if len(found) == 0:
        return end_date
    return found.index[0]

day = timedelta(days=1)

days = np.arange(start_date, end_date, day)
dones = np.zeros(days.shape, dtype='bool')

df = np.array([days, dones]).transpose()
df = pd.DataFrame(df, columns=['day','downloaded'])
df = df.set_index('day')

files = glob.glob(os.path.join(download_dir, '*.grib'))

for file in files:
    fname = os.path.basename(file)
    fname = os.path.splitext(fname)[0]
    fname = fname.split('_')
    period_start = datetime.strptime(fname[0], '%Y%m%d') # inclusive
    period_end = datetime.strptime(fname[1], '%Y%m%d') # inclusive
    df[period_start:period_end] = True
    
periods = []
#date = start_date
for date, row in df.iterrows():
    if row.downloaded:
        continue

    # the next day that has already been downloaded
    next_downloaded = get_next_downloaded(df, date)
    # pretend the next month has already been downloaded
    # so we're only fetching one month at a time from MARS
    next_downloaded = min(next_downloaded, get_next_month(date))
    
    period_start = date
    while period_start < next_downloaded:
        period_delta = min(next_downloaded - period_start, max_period)
        period_end = period_start + period_delta - day # inclusive
        periods.append('{:04}{:02}{:02}_{:04}{:02}{:02}'.format(
            period_start.year, period_start.month, period_start.day,
            period_end.year, period_end.month, period_end.day))
        df[period_start:period_end] = True
        period_start += period_delta
        
assert(len(df) == sum(df.downloaded))
print('[*] {} periods to download'.format(len(periods)))
with open(list_fname, 'w') as f:
    f.write('\n'.join(periods))
print('[*] periods written to {}'.format(list_fname))

    
        
