#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 21:22:45 2020

@author: liam
"""

import cdsapi
from datetime import datetime, timedelta

from wind.drives import era5_dir

input_fpath = era5_dir + 'level135-2014-2019/list.txt'
output_dir = era5_dir + 'level135-2014-2019/rawdata/'
#level = '131' # 170 meters
#level = '133' # 100 meters
level = '135' # 60 meters?
        
def download(period):
    print('[*] requesting period={}'.format(period))
    start, end = period.split('_')
    end = datetime.strptime(end, '%Y%m%d')
    start = datetime.strptime(start, '%Y%m%d')
    args = start.year, start.month, start.day, end.year, end.month, end.day
    date_str = '{:04}-{:02}-{:02}/to/{:04}-{:02}-{:02}'.format(*args)

    c = cdsapi.Client()
    c.quiet = True
    c.retrieve('reanalysis-era5-complete', {
        'grid':'N320',
        'class': 'ea',
        'date': date_str,
        'expver': '1',
        'levelist': level,
        'levtype': 'ml',
        'param': '131/132', # only u-wind and v-wind
        'stream': 'oper',
        'time': '00:00:00/01:00:00/02:00:00/03:00:00/04:00:00/05:00:00/' + \
                '06:00:00/07:00:00/08:00:00/09:00:00/10:00:00/11:00:00/' + \
                '12:00:00/13:00:00/14:00:00/15:00:00/16:00:00/17:00:00/' + \
                '18:00:00/19:00:00/20:00:00/21:00:00/22:00:00/23:00:00',
        'type': 'an',
    }, output_dir + '{}.grib'.format(period))

with open(input_fpath, 'r') as f:
    periods = [x.strip() for x in f.readlines()]

if len(periods) == 0:
    print('[*] all periods downloaded!')
    raise(Warning)
else:
    download(periods[0])

with open(input_fpath, 'w') as f:
    f.write('\n'.join(periods[1:]))
    
    
    
    
    
