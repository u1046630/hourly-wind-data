#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 15:44:01 2020

@author: liam
"""

# extra code not currently using...




###################
### Cross table ###
###################

fig = plt.figure(figsize=(15, 6))
plt.suptitle('cross table\n' + name)

values = np.zeros((len(month_axis), len(hour_axis)))
for i in range(len(month_axis)):
    for j in range(len(hour_axis)):
        weights = np.bitwise_and(months==month_axis[i], hours==hour_axis[j])
        values[i,j] = np.average(wind, weights=weights)
values = values / np.mean(values)
x_axis, y_axis = np.mgrid[0:len(month_axis)+1, 0:len(hour_axis)+1]
ax = plt.subplot(121)
plt.title('ERA5')
plt.xlabel('month')
plt.ylabel('hour')
pc = ax.pcolormesh(x_axis, y_axis, values)
bar = fig.colorbar(pc)
bar.set_label('wind speed index')

with plots_zip.open('HourlyMonthlyData.csv') as f:
    df = pd.read_csv(f)
values = np.zeros((len(month_axis), len(hour_axis)))
for i in range(len(month_axis)):
    for j in range(len(hour_axis)):
        rows = df[(df.month==month_axis[i]) & (df.hour==hour_axis[j])]
        values[i,j] = rows.iloc[0].value
ax = plt.subplot(122)
plt.title('GWA')
plt.xlabel('month')
plt.ylabel('hour')
pc = ax.pcolormesh(x_axis, y_axis, values)
bar = fig.colorbar(pc)
bar.set_label('wind speed index')






### wind roses

plt.figure(figsize=(15,7))
plt.suptitle('wind frequency rose\n' + name)

width = 2*np.pi / 12 * 0.9

### wind frequency rose
ax = plt.subplot(111, projection='polar')

with plots_zip.open('windFrequencyRose.csv') as f:
    df = pd.read_csv(f)
radii = df.value * 100 / np.sum(df.value)
theta = df.center_degrees - 30
theta = 90 - theta
theta = theta * np.pi / 180
ax.bar(theta, radii, width=width)

theta = np.linspace(0.0, 2*np.pi, 12, endpoint=False)
radii = np.bincount(sector)
radii = radii * 100 / np.sum(radii)
ax.bar(theta, radii, width=width*0.5)

ax.legend(['GWA', 'ERA5'])

### wind speed rose (average direction multiplied by time in that direction)
ax = plt.subplot(122, projection='polar')

for i in range(12):
    radii[i] = np.sum(wind * (bucket==i))
radii = radii * 100 / np.sum(radii)
ax.bar(theta, radii, width=width)

with plots_zip.open('windSpeedRose.csv') as f:
    df = pd.read_csv(f)
radii = df.value * 100 / np.sum(df.value)
theta = df.center_degrees - 30
theta = 90 - theta
theta = theta * np.pi / 180
ax.bar(theta, radii, width=width*0.5)
ax.legend(['ERA5', 'GWA'])
plt.title('wind speed rose')