#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 14:12:34 2020

@author: liam
"""

import numpy as np, pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats

from wind.util import read_gwc_file

#####################
### weibull funcs ###
#####################

def get_gwa_weibulls(fname):
    # values for GWA plots
    with open(fname, 'r') as f:
        roughness, heights, f_qwa, a_gwa, k_gwa = read_gwc_file(f)
    # for offshore, roughness == 0, height == 100m:
    i = np.where(roughness == 0.0)[0][0]
    j = np.where(heights == 100.0)[0][0]
    a_gwa = a_gwa[i,j,:] # weibull a parameter by sector
    k_gwa = k_gwa[i,j,:] # weibull k parameter by sector
    f_qwa = f_qwa[i,:] # frequency by sector
    
    # have to convert their sector (clockwise from north) 
    # to my sectors (anitclockwise from east)
    mapping = np.arange(3,3-12,-1)
    a_gwa = a_gwa.take(mapping)
    k_gwa = k_gwa.take(mapping)
    f_gwa = f_qwa.take(mapping)
    
    return a_gwa, k_gwa, f_gwa

def calc_era_weibulls(wind, sector):
    a_era = np.full(12, np.nan)
    k_era = np.full(12, np.nan)
    f_era = np.full(12, np.nan)
    for i in range(12):
        indices = np.where(sector == i)[0]
        f_era[i] = len(indices)
        temp = np.take(wind, indices)
        k,_,a = stats.weibull_min.fit(temp, floc=0)
        a_era[i] = a # scale parameter (roughly average wind speed in m/s)
        k_era[i] = k # shape parameter (high k means low variance of wind speeds)
    f_era = f_era / np.sum(f_era) * 100.0
    
    return a_era, k_era, f_era

def adjust_to_weibulls(wind, sector, a_gwa, k_gwa, a_era, k_era):
    new_wind = np.zeros(wind.shape)
    for i in range(12):
        new_wind += (sector == i)*(a_gwa[i] * ((wind / a_era[i])**(k_era[i] / k_gwa[i])))
        
    bias = np.mean(new_wind - wind)
    error = np.sqrt(np.mean((new_wind - wind - bias)**2))
    print('[*] weibull adjustment: {:.2f} +/- {:.2f} m/s'.format(bias, error))
        
    return new_wind

def adjust_boundaries_to_frequency(angle, boundaries, f_gwa, f_era):
    assert(angle.min() >= 0 and angle.max() < 360)
    
    # curve_x and curve_y form a sort of duration curve
    # point (x,y) on curve means for x% of the time angle < y
    curve_y = angle.copy()
    curve_y.sort()
    curve_x = np.linspace(0, 100, len(curve_y), endpoint=False)
    
    # boundaries are the angles between the 12 sectors on the wind rose (in degrees)
    # deltas are the amount these boundaries have to shift
    # by % of wind frequency (not angle), so use to above curve to convert back and forth
    deltas = np.cumsum(f_era - f_gwa)
    deltas -= np.mean(deltas)
    x = np.interp(boundaries, curve_y, curve_x)
    x = (x - deltas) % 100
    new_boundaries = np.interp(x, curve_x, curve_y)
    return new_boundaries
    
def calc_sectors(angle, boundaries):
    # sector1 lies between boundaries 0 and 1
    new_sector = np.zeros(angle.shape, dtype='int')
    for i0 in range(12):
        i1 = (i0 + 1)%12
        b0, b1 = boundaries[i0], boundaries[i1]
        if b0 < b1:
            new_sector += ((angle > b0) * (angle < b1)) * i1
        else:
            new_sector += ((angle > b0) + (angle < b1)) * i1
    return new_sector
    

def plot_weibulls(name, a_gwa, k_gwa, f_gwa, a_era, k_era, f_era):
    
    plt.figure(figsize=(15,6))
    plt.suptitle('Weibull distributions from GWC file\n' + name)
    width = 2*np.pi / 12 * 0.9
    theta = np.linspace(0.0, 2*np.pi, 12, endpoint=False)
    
    ax = plt.subplot(131, projection='polar')
    ax.bar(theta, a_gwa, width=width)
    ax.bar(theta, a_era, width=width*0.5)
    plt.title('Weibull A parameter')
    plt.legend(['GWA', 'ERA5'])
    
    ax = plt.subplot(132, projection='polar')
    ax.bar(theta, k_gwa, width=width)
    ax.bar(theta, k_era, width=width*0.5)
    plt.title('Weibull K parameter')
    plt.legend(['GWA', 'ERA5'])
    
    ax = plt.subplot(133, projection='polar')
    ax.bar(theta, f_gwa, width=width)
    ax.bar(theta, f_era, width=width*0.5)
    plt.title('Frequency')
    plt.legend(['GWA', 'ERA5'])
    

######################
### temporal funcs ###
######################
    
def calc_era_temporals(wind, time):
    
    hours = time.hour
    months = time.month
    years = time.year
    hour_vect = np.arange(hours.min(), hours.max() + 1)
    month_vect = np.arange(months.min(), months.max() + 1)
    year_vect = np.arange(years.min(), years.max() + 1)
    
    hour_era = np.zeros(hour_vect.shape)
    for i in range(len(hour_vect)):
        hour_era[i] = np.average(wind, weights=hours==hour_vect[i])
    hour_era = hour_era / np.mean(hour_era)
    
    month_era = np.zeros(month_vect.shape)
    for i in range(len(month_vect)):
        month_era[i] = np.average(wind, weights=months==month_vect[i])
    month_era = month_era / np.mean(month_era)
    
    year_era = np.zeros(year_vect.shape)
    for i in range(len(year_vect)):
        year_era[i] = np.average(wind, weights=years==year_vect[i])
    year_era = year_era / np.mean(year_era)
    
    return (hour_era, hour_vect), (month_era, month_vect), (year_era, year_vect)


def get_gwa_temporals(plots_zip):
    
    with plots_zip.open('HourlyData.csv') as f:
        df = pd.read_csv(f)
        
    hour_gwa = np.array(df['value'])
    hour_vect = np.array(df['time'])
    
    with plots_zip.open('MonthlyData.csv') as f:
        df = pd.read_csv(f)
        
    month_gwa = np.array(df['value'])
    month_vect = np.array(df['time'])
    
    with plots_zip.open('AnnualData.csv') as f:
        df = pd.read_csv(f)
    year_gwa = np.array(df['value'])
    year_vect = np.array(df['time'])
    
    return (hour_gwa, hour_vect), (month_gwa, month_vect), (year_gwa, year_vect)

def plot_temporals(name, hour_gwa, month_gwa, year_gwa, hour_era, month_era, year_era):
    
    plt.figure(figsize=(15,10))
    plt.suptitle('temporal plots\n' + name)

    ax = plt.subplot(311)
    ax.plot(hour_gwa[1], hour_gwa[0], marker='o')
    ax.plot(hour_era[1], hour_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('hour')
    plt.ylabel('wind speed index')

    ax = plt.subplot(312)
    ax.plot(month_gwa[1], month_gwa[0], marker='o')
    ax.plot(month_era[1], month_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('month')
    plt.ylabel('wind speed index')

    ax = plt.subplot(313)
    ax.plot(year_gwa[1], year_gwa[0], marker='o')
    ax.plot(year_era[1], year_era[0], marker='o')
    ax.legend(['GWA', 'ERA5'])
    plt.xlabel('year')
    plt.ylabel('wind speed index')
    

def calc_era_crosstable(wind, time):
    
    hour_vect = np.arange(0, 24)
    month_vect = np.arange(1, 12+1)
    
    crosstable_era = np.zeros((len(month_vect), len(hour_vect)))
    for i in range(len(month_vect)):
        for j in range(len(hour_vect)):
            weights = np.bitwise_and(time.month==month_vect[i], time.hour==hour_vect[j])
            crosstable_era[i,j] = np.average(wind, weights=weights)
    crosstable_era = crosstable_era / np.mean(crosstable_era)
    
    return crosstable_era

def get_gwa_crosstable(plots_zip):
    
    hour_vect = np.arange(0, 24)
    month_vect = np.arange(1, 12+1)
    
    with plots_zip.open('HourlyMonthlyData.csv') as f:
        df = pd.read_csv(f)
    crosstable_gwa = np.zeros((len(month_vect), len(hour_vect)))
    for i in range(len(month_vect)):
        for j in range(len(hour_vect)):
            rows = df[(df.month==month_vect[i]) & (df.hour==hour_vect[j])]
            crosstable_gwa[i,j] = rows.iloc[0].value
            
    return crosstable_gwa

def plot_crosstables(name, crosstable_gwa, crosstable_era):
    
    x_axis, y_axis = np.mgrid[:12+1,:24+1]

    fig = plt.figure(figsize=(15, 6))
    plt.suptitle('cross table\n' + name)
    
    ax = plt.subplot(121)
    pc = ax.pcolormesh(x_axis, y_axis, crosstable_gwa)
    plt.title('GWA')
    plt.xlabel('month')
    plt.ylabel('hour')
    bar = fig.colorbar(pc)
    bar.set_label('wind speed index')
    
    ax = plt.subplot(122)
    pc = ax.pcolormesh(x_axis, y_axis, crosstable_era)
    plt.title('ERA5')
    plt.xlabel('month')
    plt.ylabel('hour')
    bar = fig.colorbar(pc)
    bar.set_label('wind speed index')
    
def adjust_to_crosstable(wind, time, crosstable_gwa, crosstable_era):
    
    new_wind = np.zeros(wind.shape)
    hour_vect = np.arange(0, 24)
    month_vect = np.arange(1, 12+1)
    
    for i in range(len(month_vect)):
        for j in range(len(hour_vect)):
            mask = (time.month == month_vect[i]) * (time.hour == hour_vect[j])
            new_wind += mask * wind * (crosstable_gwa[i,j] / crosstable_era[i,j])
        
    bias = np.mean(new_wind - wind)
    error = np.sqrt(np.mean((new_wind - wind - bias)**2))
    print('[*] crosstable adjustment: {:.2f} +/- {:.2f} m/s'.format(bias, error))
            
    return new_wind
    
def get_gwa_mean(plots_zip, wind):
    with plots_zip.open('windSpeed.csv') as f:
        df = pd.read_csv(f)
    return df[df.sel_perc == 50].iloc[0].val

def adjust_to_mean(wind, mean_gwa, mean_era):
    new_wind = wind * (mean_gwa / mean_era)
    bias = np.mean(new_wind - wind)
    error = np.sqrt(np.mean((new_wind - wind - bias)**2))
    print('[*] mean adjustment: {:.2f} +/- {:.2f} m/s'.format(bias, error))
    return new_wind