#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 09:13:46 2020

@author: liam
"""

from zipfile import ZipFile
import glob, sys
import pandas as pd, numpy as np
from datetime import datetime, timedelta

from wind.drives import globalwindatlas_dir, thewindpower_dir
from wind.access_era5 import get_era5_data

from funcs import *

###############
### globals ###
###############

name = 'London Array'
country = 'uk'
date_start = datetime(2008, 1, 1)
date_end = datetime(2018, 1, 1)
do_plots = True

wind_farm_fname = thewindpower_dir + 'countries/{}/wind-farm-data.csv'.format(country)
plots_fname = glob.glob(globalwindatlas_dir + '{}/{}/gwa-plot-data*.zip'.format(country, name))[0]
gwc_fname = glob.glob(globalwindatlas_dir + '{}/{}/gwa3_gwc*.lib'.format(country, name))[0]
output_fname = globalwindatlas_dir + '{}/{}/adjusted_era5'.format(country, name)

############
### main ###
############

farms = pd.read_csv(wind_farm_fname, index_col='Name')
farm = farms.loc[name]

# get ERA5 wind data
u_wind = get_era5_data(date_start, date_end, 133, 'u', farm.Latitude, farm.Longitude, interpolated=True)
v_wind = get_era5_data(date_start, date_end, 133, 'v', farm.Latitude, farm.Longitude, interpolated=True)
wind = (u_wind**2 + v_wind**2)**0.5 # magnitude
wind2 = wind.copy()

# angle: direction wind is coming from in degrees, 0 is from east, 90 is from north
# boundaries: angles that define wind rose sectors
# sectors: 0 to 11 is east then anticlockwise
# time: datetime object for each hour
angle = (np.degrees(np.arctan(v_wind/u_wind)) + 180.0 * (u_wind > 0.0)) % 360.0
boundaries = np.linspace(360/24, 360/24+360, 12, endpoint=False)
sector = calc_sectors(angle, boundaries)
time = pd.DatetimeIndex(np.arange(date_start, date_end, timedelta(hours=1))) 

#####################
### get GWA stats ###
#####################

with ZipFile(plots_fname) as plots_zip:
    a_gwa, k_gwa, f_gwa = get_gwa_weibulls(gwc_fname)
    hour_gwa, month_gwa, year_gwa = get_gwa_temporals(plots_zip)
    crosstable_gwa = get_gwa_crosstable(plots_zip)
    mean_gwa = get_gwa_mean(plots_zip, wind)

#################
### plot orig ###
#################

if do_plots:
    a_era, k_era, f_era = calc_era_weibulls(wind, sector)
    plot_weibulls(name, a_gwa, k_gwa, f_gwa, a_era, k_era, f_era)
    hour_era, month_era, year_era = calc_era_temporals(wind, time)
    plot_temporals(name, hour_gwa, month_gwa, year_gwa, hour_era, month_era, year_era)
    crosstable_era = calc_era_crosstable(wind, time)
    plot_crosstables(name, crosstable_gwa, crosstable_era)

##############
### adjust ###
##############

# adjust the mean wind speed
#mean_era = np.mean(wind)
#wind = adjust_to_mean(wind, mean_gwa, mean_era)

# shift the boundaries between sectors on wind rose so that wind frequency rose matches
a_era, k_era, f_era = calc_era_weibulls(wind, sector)
boundaries = adjust_boundaries_to_frequency(angle, boundaries, f_gwa, f_era)
sector = calc_sectors(angle, boundaries)

# adjust wind speed to match the GWA's Weibull distribution for each sector
a_era, k_era, f_era = calc_era_weibulls(wind, sector)
wind = adjust_to_weibulls(wind, sector, a_gwa, k_gwa, a_era, k_era)

# adjust the wind speed per month and hour, to match GWA's crosstable plot
crosstable_era = calc_era_crosstable(wind, time)
wind = adjust_to_crosstable(wind, time, crosstable_gwa, crosstable_era)

##################
### plot after ###
##################

if do_plots:
    a_era, k_era, f_era = calc_era_weibulls(wind, sector)
    plot_weibulls(name, a_gwa, k_gwa, f_gwa, a_era, k_era, f_era)
    hour_era, month_era, year_era = calc_era_temporals(wind, time)
    plot_temporals(name, hour_gwa, month_gwa, year_gwa, hour_era, month_era, year_era)
    crosstable_era = calc_era_crosstable(wind, time)
    plot_crosstables(name, crosstable_gwa, crosstable_era)

np.savez(output_fname, wind=wind, time=time)
