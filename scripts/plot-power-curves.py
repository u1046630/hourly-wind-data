#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 12:06:26 2020

@author: liam
"""

import numpy as np, pandas as pd
import matplotlib.pyplot as plt

from modules.power_curves import get_power_curve

power_curves_path = '../rawdata/power-curves.csv'
turbine = 'Vestas V112-3.45'
k = 1.0
sigma = 0.00001 # m/s

xp1, fp1 = get_power_curve(turbine, 0.9, sigma)
xp2, fp2 = get_power_curve(turbine, 1.0, sigma)
xp3, fp3 = get_power_curve(turbine, 1.1, sigma)

fig = plt.figure(figsize=(8, 5))
plt.plot(xp1, fp1)
plt.plot(xp2, fp2)
plt.plot(xp3, fp3)
plt.xlabel('wind speed (m/s)')
plt.xlim([0,18])
plt.ylabel('power')
plt.legend(['{} (k={})'.format(turbine, x) for x in (0.9, 1.0, 1.1)])
plt.title('power curves for various k')
