#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 12:06:26 2020

@author: liam
"""

# when someone requests wind data for certain coords in CSV, 
# this is the script to uses

import numpy as np, pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from wind.access_era5 import get_era5_data
from wind.power_curves import get_power_curve

turbine = 'Siemens SWT-3.6-107'
date_start = datetime(1980, 1, 1)
date_end = datetime(2020, 1, 1)
time_step = timedelta(hours=1)
level = 131 # 170m
k = 1.2 # m/s
sigma = 2.3 # m/s

power_dir = '../data/power-10yrs/'
wind_gen_path = '../rawdata/wind-generators.csv'

input_fname = '../input/bolivia-coords.csv'
output_dir = '../output/bolivia-coords/'

############
### main ###
############

coords = pd.read_csv(input_fname)
xp, fp = get_power_curve(turbine, k, sigma)
fp /= fp.max()
time_vect = np.arange(date_start, date_end, time_step)

pd.DataFrame({'time (UTC)': time_vect}).to_csv(f'{output_dir}datetime.csv', index=False)

for index, row in coords.iterrows():
    
    if row.Lat[-1] == 'N':
        lat = float(row.Lat[:-1])
    elif row.Lat[-1] == 'S':
        lat = -float(row.Lat[:-1])
    else:
        raise Exception('latitude must be "N" or "S"')
    
    if row.Lon[-1] == 'E':
        lon = float(row.Lon[:-1])
    elif row.Lon[-1] == 'W':
        lon = -float(row.Lon[:-1])
    else:
        raise Exception('longitude must be "E" or "W"')
    
    # load up ERA5 wind and output power data, remove point where power = nan
    u_wind = get_era5_data(date_start, date_end, level, 'u', lat, lon, interpolated=True)
    v_wind = get_era5_data(date_start, date_end, level, 'v', lat, lon, interpolated=True)
    wind = (u_wind**2 + v_wind**2)**0.5
    power = np.interp(wind, xp, fp)
    
    df = pd.DataFrame({
        #'time (UTC)': time_vect,
        'wind speed (m/s)': wind,
        'capacity factor': power, 
    })
    #df = df.set_index('time (UTC)')
    df.to_csv(f'{output_dir}{index+1}_{row.Lat}_{row.Lon}.csv'.format(index), float_format='%.3f', index=False)
    
    
    
