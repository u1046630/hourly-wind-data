#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 14:16:04 2020

@author: liam
"""

# this script downloads the data found here: https://test.bmreports.com/bmrs/?q=actgenration/actualgeneration
# "Actual Generation Output Per Generation Unit" is half hourly MW production by generator
# under the BMRS API this is known as report 1610 (hence the api_url)

import urllib.request
from datetime import datetime, timedelta

from wind.drives import elexon_dir

date_start = datetime(2010, 1, 1) # inclusive
date_end = datetime(2020, 1, 1) # exclusive
output_dir = elexon_dir + 'generation-raw/'
api_url = 'https://api.bmreports.com/BMRS/B1610/V2'
script_key = 'jm03lkeev3qe9op'

params = {
    'Period': '*', # all 48 half hour periods
    'ServiceType': 'csv',
    'SettlementDate': None, # in YYYY-MM-DD format
    'APIKey': script_key
}

def make_url(base, params):
    return base + '?' + '&'.join(['{}={}'.format(k, v) for k, v in params.items()])

# go backwards, stop when no data avaliable
date = date_end - timedelta(days=1)
while date >= date_start:
    date_str = '{:04}-{:02}-{:02}'.format(date.year, date.month, date.day)
    date -= timedelta(days=1)
    
    print('[*] downloading data for {}'.format(date_str))
    params['SettlementDate'] = date_str
    url = make_url(api_url, params)
    fname = output_dir + date_str + '.csv'
    
    response = urllib.request.urlopen(url)
    if response.status != 200:
        print('[!] response code {} for url {}'.format(response.status, url))
        raise(Warning)
    content = response.read()
    if content[:5] == b'<?xml':
        if b'Success But No data available' in content:
            print('[!] Success But No data available. skipping')
            continue
        else:
            print('[!] Unknown XML response: {}'.format(content))
            raise(Warning)
    
    fname = output_dir + date_str + '.csv'
    #print('[*] writing download to {}'.format(fname))
    with open(fname, 'wb') as f:
        f.write(content)
    
    
