#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 16:05:42 2020

@author: liam
"""

import numpy as np, pandas as pd
import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import optimize, ndimage, stats

from wind.access_era5 import get_era5_data
from wind.drives import elexon_dir, thewindpower_dir, globalwindatlas_dir

###############
### globals ###
###############

level = 133 # 106m

power_fname = elexon_dir + 'generation-processed/{}.npy'
power_curve_fname = thewindpower_dir + 'turbines/power-curve-data/{}.csv'
wind_farm_fname = thewindpower_dir + 'countries/uk/wind-farm-data.csv'
name_uid_fname = elexon_dir + 'metadata/thewindpowernames.csv'

date_start = datetime(2010, 1, 1)
date_end = datetime(2020, 1, 1)

fixed_params = [1.5] # sigma (m/s)

#############
### funcs ###
#############

def no_intercept_fit(x,y):
    def func(x,m):
        return x*m
    params,_ = optimize.curve_fit(func,x,y,p0=[1.0])
    return params[0]

def adjust_power_curve(x, y, params):
    wake_loss,sigma = params
    x = x.copy()
    y = y.copy()
    x += wake_loss
    step = x[1] - x[0] # distance in m/s between pts
    sigma /= step # convert sigma from m/s to steps
    y = ndimage.filters.gaussian_filter1d(y, sigma, mode='nearest')
    return x, y

def fit_power_curve(wind, power, curve_x, curve_y, fixed):
    def func(x, *params):
        params = list(params)
        params.extend(fixed)
        curve_x2, curve_y2 = adjust_power_curve(curve_x, curve_y, params)
        return np.interp(x, curve_x2, curve_y2)
    
    params,_ = optimize.curve_fit(func, wind, power, p0=[0.0])
    params = list(params)
    params.extend(fixed)
    return params

def plot_wind_power(wind, power, curves):
    plt.scatter(wind, power, alpha=0.8, s=0.3)
    colors = ['black', 'brown']
    for (curve_x, curve_y), color in zip(curves, colors):
        plt.plot(curve_x, curve_y, color=color)
    
    plt.xlim([0, 20])
    plt.xlabel('wind speed (m/s)')
    plt.ylabel('power (MW)')
        
    
def plot_power_power(power_predicted, power):
    p1,p0,R,_,_ = stats.linregress(power_predicted, power)
    
    # no intercept fit:
    p1 = no_intercept_fit(power_predicted, power)
    p0 = 0
    
    line_x = power_predicted.min(), power_predicted.max()
    line_y = p0 + line_x[0]*p1, p0 + line_x[1]*p1
    
    err = np.interp(power_predicted, line_x, line_y) - power
    err = np.sqrt(np.mean(err**2)) # root-mean-squared
    err = err*100/power.max()
    
    plt.scatter(power_predicted, power, alpha=0.5, s=0.3)
    plt.plot(line_x, line_y, color='black')
    plt.xlabel('ERA5 power (MW)')
    plt.ylabel('Actual power (MW)')
    plt.legend(['y = {:.2f}x + {:.2f}\nR2={:.2f}\nerr={:.2f}%'.format(p1, p0, R**2, err)])
    
    return {'m': p1, 'b': p0, 'R2': R**2, 'err (%)': err}
    
        
############
### main ###
############

farm_names = pd.read_csv(name_uid_fname)    
farm_data = pd.read_csv(wind_farm_fname)
farm_data = farm_data.set_index('Name')

#results = []

for _,row in farm_names.iterrows():
    uid_list = row['Elexon UID']    
    names = row['thewindpower name'].split(',')
    long_name = ' + '.join(names)
    
    if long_name != 'Ormonde':
        continue
    
    if not isinstance(uid_list, str):
        #print('[!] {}: No UIDs provided'.format(long_name))
        continue # no UID provided
    uids = uid_list.split(',')
        
    farm0 = farm_data.loc[names[0]]
    
    num_turbines = int(farm0['Number turbines'])
    turbine_model = farm0['Turbine model']
    lat, lon = farm0['Latitude'], farm0['Longitude']
    
    turbine_model = farm0['Turbine model']
    turbine_scale_factor = 1.0
    fname = power_curve_fname.format(turbine_model)
    if not os.path.exists(fname):
        if not isinstance(row['Alternate Turbine'], str):
            print('[!] {}: No alternate turbine to {} provided'.format(long_name, turbine_model))
            continue # no power curve avaliable
        turbine_model = row['Alternate Turbine']
        fname = power_curve_fname.format(turbine_model)
        if not os.path.exists(fname):
            print('[!] {}: No power curve for alternate turbine model {}'.format(long_name, turbine_model))
            continue # no power curve avaliable
        turbine_scale_factor = row['Factor']
    power_curve = pd.read_csv(fname)
    curve_x = np.array(power_curve['Wind speed (m/s)'])
    curve_y = np.array(power_curve['Power (kW)']) * turbine_scale_factor
    
    # aggregate num_turbines from multiple farms
    for name in names[1:]:
        farm = farm = farm_data.loc[name]
        num_turbines += int(farm['Number turbines'])
        assert(lat == farm['Latitude'])
        assert(lon == farm['Longitude'])
        assert(turbine_model == farm['Turbine model'])

    curve_y *= num_turbines / 1000 # MW
        
    u_wind = get_era5_data(date_start, date_end, level, 'u', lat, lon, interpolated=True)
    v_wind = get_era5_data(date_start, date_end, level, 'v', lat, lon, interpolated=True)
    wind = (u_wind**2 + v_wind**2)**0.5
    
    powers = []
    for uid in uids:
        fname = power_fname.format(uid)
        if not os.path.exists(fname):
            raise(Warning) # no processed generation data provided
        powers.append(np.load(fname))
    power = np.sum(np.array(powers), axis=0)
    
    time = np.arange(date_start, date_end, timedelta(hours=1))
    data = pd.DataFrame(zip(wind, power), columns=('wind','power'), index=time)
    data = data.dropna(how='any')
    del u_wind, v_wind, wind, power, time
    
    if len(data) == 0:
        print('[!] {}: Power data is empty'.format(long_name))
        continue
    
    curves = [(curve_x, curve_y)]
    params = fit_power_curve(data.wind, data.power, curve_x, curve_y, fixed_params)
    wake_loss, sigma = params
    curves.append(adjust_power_curve(curve_x, curve_y, params))
    
    data['predicted'] = np.interp(data.wind, curves[1][0], curves[1][1])
    
    data = data.dropna(how='any')
    
    title = '{}\n{}\nturbines: {} x {}'.format(
            long_name, ' + '.join(uids), num_turbines, turbine_model
    )
        
    fig = plt.figure(figsize=(8,5))
    plt.title(title)
    plot_wind_power(data.wind, data.power, curves)
    plt.legend(['Original','Adjusted (loss={:.1f}m/s, sigma={:.1f}m/s)'.format(wake_loss, sigma)])
    #fig.savefig('../../output/uk-offshore-wakeloss/' + long_name + '.png', bbox_inches='tight')
    #plt.close()
    
    for freq in ('H','D','W','M'):
        subdata = data.groupby(pd.Grouper(freq=freq)).mean()
        subdata = subdata.dropna(how='any')
        
        #results.append({'name': long_name})
        
        fig = plt.figure(figsize=(8,5))
        plt.title(title)
        temp = plot_power_power(subdata.predicted, subdata.power)
        #fig.savefig('../../output/uk-offshore-wakeloss/' + long_name + '-' + chunk_name + '.png', bbox_inches='tight')
        #plt.close()

    #results[-1].update(temp)    
        
#df = pd.DataFrame(results)
#df.to_csv(chunk_name + '.csv')
        