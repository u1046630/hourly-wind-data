#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:02:40 2020

@author: liam
"""

### TODO:
# I'm ignoring daylight savings issues
# that's why there's 50 half hour periods in a day

import numpy as np, pandas as pd, os
from datetime import datetime, timedelta
import progressbar

from wind.drives import elexon_dir

name_uid_fname = elexon_dir + 'metadata/thewindpowernames.csv'
input_dir = elexon_dir + 'generation-raw/'
output_dir = elexon_dir + 'generation-processed/'

date_start = datetime(2010, 1, 1) # inclusive
date_end = datetime(2020, 1, 1) # exclusive

############
### main ###
############

uid_lists = pd.read_csv(name_uid_fname)['Elexon UID']
uids = [] # uids to process

for uid_list in uid_lists:
    if not isinstance(uid_list, str):
        continue
    uids.extend(uid_list.split(','))
            

ndays = (date_end - date_start) // timedelta(days=1)
results = {}

for uid in uids:
    results[uid] = np.full(24*ndays, np.nan)
    
for iday in progressbar.progressbar(range(ndays)):
    
    date = date_start + timedelta(days=iday)
    date_str = '{:04}-{:02}-{:02}'.format(date.year, date.month, date.day)
    fname = input_dir + date_str + '.csv'
    
    if not os.path.exists(fname):
        continue
    #print('[*] {}'.format(fname))
    
    data = pd.read_csv(fname, skiprows=1)
    
    for uid in uids:
        # get the data for a particular generator
        subdata = data[data['BM Unit ID'] == uid]
        indices = np.array(subdata['SP']) - 1 # SP: hourly periods
        values = np.array(subdata['Quantity (MW)']) # output in MW
        
        # fill array with avaliable data, leave unavaliable data as np.nan:
        power = np.full((50,), np.nan) 
        np.put(power, indices, values)
        # average half hourly power to hourly, and ignore 25th hour
        power = np.nanmean(power.reshape((25,2)), axis=1)[:24]
        
        # insert into results, ignore 25th hour
        results[uid][iday*24 : (iday+1)*24] = power
    
    
for uid in uids:
    np.save(output_dir + uid, results[uid])


