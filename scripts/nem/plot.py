#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 16:05:42 2020

@author: liam
"""

import numpy as np, pandas as pd
import sys, os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import optimize

from wind.access_era5 import get_era5_data
from wind.drives import elexon_dir, thewindpower_dir

###############
### globals ###
###############

power_fname = elexon_dir + 'generation-processed/{}.npy'
power_curve_fname = thewindpower_dir + 'turbines/power-curve-data/{}.csv'
wind_farm_fname = thewindpower_dir + 'countries/uk/wind-farm-data.csv'
name_uid_fname = elexon_dir + 'metadata/thewindpowernames.csv'

date_start = datetime(2010, 1, 1)
date_end = datetime(2020, 1, 1)

#############
### funcs ###
#############

def plot_wind_power(wind, power, curve_x, curve_y):
    plt.scatter(wind, power, alpha=0.8, s=0.2)
    plt.plot(curve_x, curve_y, color='black')
    plt.xlim([0, 20])
    plt.xlabel('wind speed (m/s)')
    plt.ylabel('power (MW)')
    
    def func(x, wake_loss):
        return np.interp(x, curve_x + wake_loss, curve_y)
    
    params,_ = optimize.curve_fit(func, wind, power, p0=[1.0])
    curve_x2 = curve_x + params[0]
    wake_loss = params[0] #(params[0] - 1)*100
    plt.plot(curve_x2, curve_y, color='brown')
    plt.legend(['Original','Adjusted (loss={:.1f}m/s)'.format(wake_loss)])
        
############
### main ###
############

farm_names = pd.read_csv(name_uid_fname)    
farm_data = pd.read_csv(wind_farm_fname)
farm_data = farm_data.set_index('Name')

for _,row in farm_names.iterrows():
    uid_list = row['Elexon UID']    
    names = row['thewindpower name'].split(',')
    long_name = ' + '.join(names)
    
    #if long_name != 'Sheringham Shoal':
    #    continue
    
    if not isinstance(uid_list, str):
        #print('[!] {}: No UIDs provided'.format(long_name))
        continue # no UID provided
    uids = uid_list.split(',')
        
    farm0 = farm_data.loc[names[0]]
    
    num_turbines = int(farm0['Number turbines'])
    turbine_model = farm0['Turbine model']
    lat, lon = farm0['Latitude'], farm0['Longitude']
    
    turbine_model = farm0['Turbine model']
    turbine_scale_factor = 1.0
    fname = power_curve_fname.format(turbine_model)
    if not os.path.exists(fname):
        if not isinstance(row['Alternate Turbine'], str):
            print('[!] {}: No alternate turbine to {} provided'.format(long_name, turbine_model))
            continue # no power curve avaliable
        turbine_model = row['Alternate Turbine']
        fname = power_curve_fname.format(turbine_model)
        if not os.path.exists(fname):
            print('[!] {}: No power curve for alternate turbine model {}'.format(long_name, turbine_model))
            continue # no power curve avaliable
        turbine_scale_factor = row['Factor']
    power_curve = pd.read_csv(fname)
    curve_x = np.array(power_curve['Wind speed (m/s)'])
    curve_y = np.array(power_curve['Power (kW)']) * turbine_scale_factor
    
    # aggregate num_turbines from multiple farms
    for name in names[1:]:
        farm = farm = farm_data.loc[name]
        num_turbines += int(farm['Number turbines'])
        assert(lat == farm['Latitude'])
        assert(lon == farm['Longitude'])
        assert(turbine_model == farm['Turbine model'])

    curve_y *= num_turbines / 1000 # MW
        
    u_wind = get_era5_data(date_start, date_end, lat, lon, 'u', interpolated=True)
    v_wind = get_era5_data(date_start, date_end, lat, lon, 'v', interpolated=True)
    wind = (u_wind**2 + v_wind**2)**0.5
    
    powers = []
    for uid in uids:
        fname = power_fname.format(uid)
        if not os.path.exists(fname):
            raise(Warning) # no processed generation data provided
        temp = np.load(fname)
        if np.all(np.isnan(temp)):
            print('[!] warning: {} data is empty'.format(uid))
        powers.append(temp)
    power = np.sum(np.array(powers), axis=0)
    if np.sum(np.isnan(power)) == power.shape[0]:
        print('[!] {}: Power data is empty'.format(long_name))
        continue # data is actually empty
        
    indices = np.where(~np.isnan(power))[0]
    power = np.take(power, indices)
    wind = np.take(wind, indices)
    
    fig = plt.figure(figsize=(15, 10))
    plt.title('{}\n{}\nturbines: {} x {}'.format(
            long_name, 
            ' + '.join(uids),
            num_turbines, turbine_model))
    plot_wind_power(wind, power, curve_x, curve_y)
    fig.savefig('../../output/uk-offshore-wakeloss' + long_name + '.png', bbox_inches='tight')
    plt.close()
        
        
        
        