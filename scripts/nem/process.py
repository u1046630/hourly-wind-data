#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 11:22:33 2020

@author: liam
"""
# extract hourly wind farm power data in csv format from NEM/ARENA
# (have to average 5min data pts)
# run time ~= 6 minutes for the 42 NEM wind farms

import pandas as pd, numpy as np, os, time
from datetime import datetime, timedelta

###############
### globals ###
###############

wind_gen_path = '../rawdata/wind-generators.csv'
input_dir = '../rawdata/aremi-power-data/'
output_dir = '../data/power-10yrs/'

ignore_zero = True
step = timedelta(hours=1)

#############
### funcs ###
#############

start = datetime(2010, 1, 1) #inclusive
end = datetime(2020, 1, 1) # exclusive

def average_timesteps(data, period_start, period_end, step):
    
    N = int((period_end - period_start)/step)
    results = np.empty(N)
    results[:] = np.nan
    
    i = 0
    now = period_start
    while now < period_end:
        power = np.mean(data[(data.index >= now) & (data.index < now + step)].Power)
        if power > 0: # there is data and it's not all 0
            results[i] = power
        i += 1
        now += step
        
    return results

############
### main ###
############

t00 = time.time()
df = pd.read_csv(wind_gen_path)
df = df.sort_values('DUID')

start -= step/2
end -= step/2

for index, farm in df.iterrows():
    
    t0 = time.time()   
    uid = farm.DUID
    print('[*] {}...'.format(uid))
    
    #if uid != 'HDWF1':
    #    continue

    output_fname = output_dir + uid + '.npy'
    if os.path.exists(output_fname):
        print('       output file already exists')
        continue
    
    data = pd.read_csv(input_dir + uid, names=['Time', 'Power'], header=0, parse_dates=['Time'])
    
    if len(data) == 0:
        print('       no data avaliable')
        continue        
    
    data.Time = data.Time.dt.tz_convert(None) # remove timezone
    data.set_index('Time', inplace=True) # index by Time for speed
    data = data[(data.index >= start) & (data.index < end)] # crop for speed
    
    results = average_timesteps(data, start, end, step)
    
    if np.sum(~np.isnan(results)) == 0:
        print('       no data avaliable in this time period')
        continue
    
    print('       averaged power over hours')
    np.save(output_fname, results)
    print('       saved numpy array to {}'.format(output_fname))
    print('       {:.1f}s'.format(time.time() - t0))
    
print('[*] done ({:.1f}s)'.format(time.time() - t00))
    
    
