#! /usr/bin/python3

import pandas as pd
import urllib.request

wind_gen_path = '../rawdata/wind-generators.csv'
url = 'https://services.aremi.data61.io/aemo/v6/duidcsv/{}'
output_path = '../rawdata/aremi-power-data/'

df = pd.read_csv(wind_gen_path)
uids = list(df.DUID)

for uid in uids:
    print('[*] retrieving power data for {}'.format(uid))
    urllib.request.urlretrieve(url.format(uid), output_path + uid)
