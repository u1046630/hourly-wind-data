#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020
@author: liam
"""

import pandas as pd, numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from funcs_plot import plot_duration_curve, plot_step_changes, plot_daily_cf, plot_monthly_cf
from funcs_data import load_all
from funcs_transform import fit_timeseries, fit_duration_curve, predict_power
from funcs_util import setup

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2010, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'
    
fig, axes = plt.subplots(1, 1, figsize=(5, 5))
if isinstance(axes, np.ndarray):
    axes = list(axes.flatten())
if not isinstance(axes, list):
    axes = [axes]

for trace_name, trace_config in config_traces.items():
    
    if not trace_name.startswith('germany_tennet'):
        continue
    #if trace_config['country'] != 'denmark':
    #    continue
    #print(trace_name)
    
    data, data_curves = load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level)
    
    params, which = setup('wakeloss sigma')
    params, _ = fit_timeseries(trace_config, data, data_curves, params, which)
    data['cf_predicted'] = predict_power(trace_config, data, data_curves, params, which)
    title = trace_name + ': ' + ', '.join([f'{x:.2f}' for x in params])
    plot_duration_curve(data, axes.pop(), title)
    
    if len(axes) == 0:
        break