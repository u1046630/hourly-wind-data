#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020
@author: liam
"""

import pandas as pd, numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from funcs_plot import plot_duration_curve, plot_step_changes, plot_daily_cf, plot_monthly_cf
from funcs_data import load_era5, load_entsoe, load_countryinfo, load_thewindpower
from funcs_transform import fit_timeseries, fit_duration_curve, predict_power
from funcs_util import setup
from funcs_error import rmse_timeseries

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2010, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'

for trace_name, trace_config in config_traces.items():
    
    if not trace_name.startswith('London Array 4'):
        continue
    
    data_entsoe = load_entsoe(trace_config, traces_dirs)
    
    data_entsoe = data_entsoe[(data_entsoe.index >= era5_date_start) & (data_entsoe.index < era5_date_end)]
    date_start = data_entsoe.index[0]
    date_end = data_entsoe.index[-1] + timedelta(hours=1)
    data_farms = load_countryinfo(trace_config)
    data_capacity, data_curves = load_thewindpower(trace_config, data_farms, date_start, date_end)
    
    lat, lon = 51.6259722222222, 1.49597222222222
    results = np.full((7, 7), np.nan)
    for i, lat_prime in enumerate(np.linspace(-0.3, 0.3, 7, endpoint=True)):
        for j, lon_prime in enumerate(np.linspace(-0.3, 0.3, 7, endpoint=True)):
            
            trace_config['farms']['London Array']['Latitude'] = lat + lat_prime
            trace_config['farms']['London Array']['Longitude'] = lon + lon_prime
    
            data_farms = load_countryinfo(trace_config)
            data_era5 = load_era5(trace_config, data_farms, date_start, date_end, level)
            data = data_entsoe.join([data_era5, data_capacity])
            params, which = setup('wakeloss')
            params, _ = fit_duration_curve(trace_config, data, data_curves, params, which)
            data['cf_predicted'] = predict_power(trace_config, data, data_curves, params, which)
            
            results[i,j] = rmse_timeseries(data)
            print('.', end='')