#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:50:11 2020

@author: liam
"""

import numpy as np, pandas as pd

def rmse_timeseries(data, predictor='cf_predicted'):
    return np.sqrt(np.nanmean((data.cf_actual - data[predictor])**2))
    
def rmse_duration(data, predictor='cf_predicted'):
    a = np.array(data.cf_actual)
    b = np.array(data[predictor])
    a[::-1].sort()
    b[::-1].sort()
    return np.sqrt(np.average((a - b)**2))

def rmse_duration2(data):
    a = np.array(data.cf_actual)
    b = np.array(data.cf_predicted)
    dcurve_y = a.copy()
    dcurve_y.sort()
    dcurve_x = np.linspace(0, 1, num=len(dcurve_y))
    err = np.interp(a, dcurve_y, dcurve_x) - np.interp(b, dcurve_y, dcurve_x)
    return np.sqrt(np.average(err**2))

def rmse_daily(data, predictor='cf_predicted'):
    return rmse_timeseries(data.groupby(pd.Grouper(freq='D')).mean(), predictor)