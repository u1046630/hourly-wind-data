#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 12:21:38 2020

@author: liam

this config file matches up wind farm descriptions listed on thewindpower.net (under names like "London Array")
with generation traces provided by ENTSO-E (under resource codes like "48W00000LARYO-1Z")

it isn't a one to one relationship. if multiple ENTSO-E generation traces map to 
one farm description, they are listed separately. if multiple farm descriptions 
map to one generation trace, their predicted power outputs are summed before comparison.
"""

from wind.drives import entsoe_dir

alternative_turbines = {
    'Adwen AD 5-135': 'Vestas V136-3450',
    'Siemens SWT-3.0-108': 'Siemens SWT-3.2-113',
    'Siemens SWT-4.0-130': 'Siemens SWT-3.6-130',
    'Siemens SWT-6.0-154': 'Siemens SWT-3.2-113',
    'Siemens SWT-6.0-120': 'Siemens SWT-2.3-82',
    'Siemens SWT-7.0-154': 'Siemens SWT-3.0-101',
    'Vestas V90-3000 Offshore': 'Vestas V90-3000',
    'Bard VM': 'Siemens SWT-3.2-113', # not that good
    'Siemens SWT-4.0-120': 'Siemens SWT-3.6-107', # not that good
    'GE Energy Haliade 150': 'Siemens SWT-3.6-107', # not that good
    'MHI Vestas Offshore V164-8400': 'Vestas V164-8000',
    'Siemens-Gamesa SG 8.0-167 DD': 'Vestas V164-8000', 
}

alternative_turbines_new = {
    'Adwen AD 5-135': 'Vestas V112-3450',
    'Siemens SWT-3.0-108': 'Vestas V100-2600',
    'Siemens SWT-4.0-130': 'Vestas V112-3000 Offshore',
    'Siemens SWT-6.0-154': 'Vestas V117-3450',
    'Siemens SWT-6.0-120': 'Vestas V66-1750',
    'Siemens SWT-7.0-154': 'Siemens SWT-3.0-101',
    'Vestas V90-3000 Offshore': 'Vestas V90-3000',
    'Bard VM': 'Siemens SWT-2.3-82',
    'Siemens SWT-4.0-120': 'Vestas V112-3450', 
    'GE Energy Haliade 150': 'Vestas V112-3300',
    'MHI Vestas Offshore V164-8400': 'Vestas V80-2000',
    'Siemens-Gamesa SG 8.0-167 DD': 'Gamesa G132-5000', 
}

traces_dirs = {
    'individual': entsoe_dir + 'processed/ActualGenerationOutputPerUnit/',
    'area': entsoe_dir + 'processed/AggregatedGenerationPerType/',
}

corrections = {
    # uk
    'Beatrice': {'Hub height (m)': 101},
    'Dudgeon': {'Hub height (m)': 110},
    'Galloper': {'Hub height (m)': 103.5},
    #'Greater Gabbard Extension': {'Turbine model': 'Siemens SWT-6.0-154', 'Hub height (m)': 105}, # hub height guess
    'Walney Extension - Part #1': {'Hub height (m)': 105},
    'Walney Extension - Part #2': {'Hub height (m)': 105},
    'Hornsea Project One - Njord': {'Commissioning': '2019/08', 'Hub height (m)': 100}, # hub height guess
    'Hornsea Project Two - Breesea and Optimus Wind': {'Hub height (m)': 150}, #hub height guess
    'Hywind Scotland Pilot Park': {'Turbine model': 'Siemens SWT-3.2-113'},
    'Walney - Part #1': {'Latitude': 54.081, 'Longitude': -3.60497222222222},
    # germany
    'Wikinger Offshore': {'Hub height (m)': 75, 'Commissioning': '2018/02'},
    'Borkum Riffgrund II': {'Hub height (m)': 105},
    'EnBW Hohe See': {'Hub height (m)': 105},
    'EnBW Baltic 1': {'Power (kW)': 20/21*48300}, # only 20 of 21 turbines
    'Arkona': {'Commissioning': '2018/11'},
    # netherlands
    'Gemini': {'Commissioning': '2016/06'},
    # belgium
    'Norther': {'Hub height (m)': 105},   
}

config_traces = {
                
    ########
    ## UK ##
    ########
    
    'Robin Rigg 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W000000RREW-14',
        'farms': [
            'Robin Rigg', 
        ],
    },
    'Robin Rigg 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W000000RRWW-1P',
        'farms': [
            'Robin Rigg',
        ],
    },
    'EOWDC': { # aka: Aberdeen Offshore Wind Farm
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000ABRBO-19',
        'farms': [
            'EOWDC - Part #1', 
            #'EOWDC - Part #2', # same turbine, same coords
        ],
    },
    'Beatrice 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BEATO-1T',
        'farms': [
            'Beatrice',
        ],
    },
    'Beatrice 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BEATO-2R',
        'farms': [
            'Beatrice',
        ],
    },
    'Beatrice 3': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BEATO-3P',
        'farms': [
            'Beatrice',
        ],
    },
    'Beatrice 4': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BEATO-4N',
        'farms': [
            'Beatrice',
        ],
    },
    'Barrow': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BOWLW-1K',
        'farms': [
            'Barrow',
        ],
    },
    'Burbo Bank 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BURBW-1L',
        'farms': [
            'Burbo Bank - Part #1',
        ],
    },
    'Burbo Bank 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000BRBEO-17',
        'farms': [
            'Burbo Bank - Part #2',
        ],
    },
    'Dudgeon 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000DDGNO-1E',
        'farms': [
            'Dudgeon',
        ],
    },
    'Dudgeon 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000DDGNO-2C',
        'farms': [
            'Dudgeon',
        ],
    },
    'Dudgeon 3': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000DDGNO-3A',
        'farms': [
            'Dudgeon',
        ],
    },
    'Dudgeon 4': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000DDGNO-48',
        'farms': [
            'Dudgeon',
        ],
    },
    'Gunfleet Sands 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000GNFSW-1H',
        'farms': [
            'Gunfleet Sands 1', # 'Power (kW)': 100000
        ],
    },
    'Gunfleet Sands 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000GNFSW-2F',
        'farms': [
            'Gunfleet Sands 2',
        ],
    },
    'Greater Gabbard 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000GRGBW-1V',
        'farms': [
            'Greater Gabbard 1', # 'Power (kW)': 167000
        ],
    },
    'Greater Gabbard 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000GRGBW-2T',
        'farms': [
            'Greater Gabbard 2', # 'Power (kW)': 167000
        ],
    },
    'Galloper': { # aka Greater Gabbard Extension
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000GRGBW-3R',
        'farms': [
            'Galloper',
        ],
    },
    'Humber Gateway': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000HMGTO-10',
        'farms': [
            'Humber Gateway', # 'Power (kW)': 108000
        ],
    },
    'Hornsea Project One - Njord': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000HOWAO-2K',
        'farms': [
            'Hornsea Project One - Njord', # 'Power (kW)': 406000
        ],
    },
    'Hornsea Project Two - Breesea and Optimus Wind': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000HOWAO-3I',
        'farms': [
            'Hornsea Project Two - Breesea and Optimus Wind', # 'Power (kW)': 406000
        ],
    },
    'Hywind': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000HYWDW-1G',
        'farms': [
            'Hywind Scotland Pilot Park',
        ],
    },
    'London Array 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LARYO-1Z',
        'farms': [
            'London Array', # 'Power (kW)': 720000
        ],
    },
    'London Array 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LARYO-2X',
        'farms': [
            'London Array', # 'Power (kW)': 720000
        ],
    },
    'London Array 3': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LARYO-3V',
        'farms': [
            'London Array', # 'Power (kW)': 720000
        ],
    },
    'London Array 4': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LARYO-4T',
        'farms': [
            'London Array', # 'Power (kW)': 720000
        ],
    },
    'Lincs 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LNCSO-1R',
        'farms': [
            'Lincs',
        ],
    },
    'Lincs 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000LNCSO-2P',
        'farms': [
            'Lincs',
        ],
    },
    'Ormonde': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000OMNDO-1J',
        'farms': [
            'Ormonde',
        ],
    },
    'Race Bank 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000RCBKO-1S',
        'farms': [
            'Race Bank',
        ],
    },
    'Race Bank 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000RCBKO-2Q',
        'farms': [
            'Race Bank',
        ],
    },
    'Rampion 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000RMPNO-17',
        'farms': [
            'Rampion',
        ],
    },
    'Rampion 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000RMPNO-25',
        'farms': [
            'Rampion',
        ],
    },
    'West of Duddon Sands 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WDNSO-1H',
        'farms': [
            'West of Duddon Sands',
        ],
    },
    'West of Duddon Sands 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WDNSO-2F',
        'farms': [
            'West of Duddon Sands', 
        ],
    },
    'Walney 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WLNYW-1A',
        'farms': [
            'Walney - Part #1',
        ],
    },
    'Walney 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WLNYO-23',
        'farms': [
            'Walney - Part #2',
        ],
    },
    'Walney Extension 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WLNYO-31',
        'farms': [
            'Walney Extension - Part #1',
        ],
    },
    'Walney Extension 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W00000WLNYO-4-',
        'farms': [
            'Walney Extension - Part #2',
        ],
    },
    'Gwynt y Mor 1': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W0000GYMRO-15O',
        'farms': [
            'Gwynt y Môr',
        ],
    },
    'Gwynt y Mor 2': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W0000GYMRO-17K',
        'farms': [
            'Gwynt y Môr',
        ],
    },
    'Gwynt y Mor 3': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W0000GYMRO-26J',
        'farms': [
            'Gwynt y Môr',
        ],
    },
    'Gwynt y Mor 4': {
        'type': 'individual',
        'country': 'uk',
        'code': '48W0000GYMRO-28F',
        'farms': [
            'Gwynt y Môr',
        ],
    },

    #############
    ## Belgium ##
    #############
    
    'Belwind II': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22W20161115----Z',
        'farms': [
            'Belwind II', # aka Nobel Wind
        ],
    },
    'Rentel': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22W20180615----H',
        'farms': [
            'Rentel',
        ],
    },
    'Norther': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22W201902151---T',
        'farms': [
            'Norther',
        ],
    },
    'Belwind I': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22WBELWIN1500271',
        'farms': [
            'Belwind I',
        ],
    },
    'Northwind': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22WNORTHW150187B',
        'farms': [
            'Northwind',
        ],
    },
    'Thorntonbank - Part #3': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22WTHORNT150237E',
        'farms': [
            #'Thorntonbank - Part #1', # this wind farm is probably included in the trace, with about 20% of the capacity, but has similar power curve and location
            'Thorntonbank - Part #3', 
        ],
    },
    'Thorntonbank - Part #2': {
        'type': 'individual',
        'country': 'belgium',
        'code': '22WTHORNT150238C',
        'farms': [
            'Thorntonbank - Part #2', # ?? 
        ],
    },
                
    #############
    ## Denmark ##
    #############
    
    'Anholt': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000046I',
        'farms': [
            'Anholt',
        ],
    },
    'Horns Rev 1': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000047G',

        'farms': [
            'Horns Rev 1',
        ],
    },
    'Horns Rev 2': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000048E',
        'farms': [
            'Horns Rev 2',
        ],
    },
    'Horns Rev 3': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000116N',
        'farms': [
            'Horns Rev 3',
        ],
    },
    'Nysted Offshore': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000044M',
        'farms': [
            'Nysted Offshore', # aka: 'Rodsand I'
        ],
    },    
    'Rodsand II': {
        'type': 'individual',
        'country': 'denmark',
        'code': '45W000000000045K',
        'farms': [
            'Rodsand II',
        ],
    },
}

#############
### areas ### (I'm just focusing on single wind farms now)
#############
    
# =============================================================================
#     'netherlands': {
#         'type': 'area',
#         'country': 'netherlands',
#         'code': '10YNL----------L',
#         'farms': [
#             'Egmond aan Zee',
#             'Eneco Luchterduinen',
#             'Gemini',
#             'Prinses Amalia',
#             'Lely',
#         ],
#     },
#     
#     'germany_50hertz': {
#         'type': 'area',
#         'country': 'germany',
#         'code': '10YDE-VE-------2',
#         'farms': [
#             #'Breitling', # don't include this one
#             'EnBW Baltic 1', 
#             'EnBW Baltic 2',
#             'Wikinger Offshore',
#             'Arkona',
#         ],
#     },
#                 
#     'germany_tennet': {
#         'type': 'area',
#         'country': 'germany',
#         'code': '10YDE-EON------1',
#         'farms': [
#             'Borkum Riffgrund II',
#             'Alpha Ventus - Part #1',
#             'Alpha Ventus - Part #2',
#             'Bard Offshore 1',
#             'Riffgat',
#             'Dan Tysk',
#             'Nordsee Ost',
#             'Meerwind Ost',
#             'Meerwind SÃ¼d',
#             'Butendiek',
#             'Global Tech I',
#             'Trianel Borkum I',
#             'Borkum Riffgrund I',
#             'Amrumbank West',
#             'Sandbank',
#             'NordergrÃ¼nde',
#             'Veja Mate',
#             'Gode Wind I',
#             'Gode Wind II',
#             'Nordsee One Offshore',
#             'Merkur Offshore',
#             'Deutsche Bucht',
#             'EnBW Hohe See',
#             'Hooksiel',
#         ],
#     },
# =============================================================================   
            
############
### duds ### (generally not enough data)
############            

# =============================================================================
#         'East Anglia One 1': {
#             'type': 'individual',
#             'country': 'uk',
#             'code': '48W000000EAAO-2P',
#             'farms': [
#                 'East Anglia One',
#             ],
#         },
#         'East Anglia One 2': {
#             'type': 'individual',
#             'country': 'uk',
#             'code': '48W000000EAAO-1R',
#             'farms': [
#                 'East Anglia One',
#             ],
#         },
#         'Hornsea Project One - Heron Wind': {
#             'type': 'individual',
#             'country': 'uk',
#             'code': '48W00000HOWAO-1M',
#             'farms': [
#                 'Hornsea Project One - Heron Wind',
#             ],
#         },
#         'Northwester 2': {
#             'type': 'individual',
#             'country': 'belgium',
#             'code': '22W201909151---M', # all 0???
#             'farms': [
#                 'Northwester 2',
#             ],
#         },
# =============================================================================