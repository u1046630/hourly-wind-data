#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020
@author: liam
"""

import pandas as pd, numpy as np, geopandas as gpd
import progressbar
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from shapely.geometry import Point

from funcs_plot import plot_duration_curve, plot_residue_diag, plot_error_duration, plot_duration_curve3, plot_daily_cf
from funcs_data import load_all
from funcs_transform import fit_timeseries, fit_duration_curve, minimise_duration_errors, predict_power
from funcs_error import rmse_duration, rmse_timeseries, rmse_duration2, rmse_daily
from funcs_util import setup, param_dict

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2014, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'

def get_result(data, data_curves, param_str, func):
    
    params, which = setup(param_str)
    if len(params) > 0:
        params, _ = func(trace_config, data, data_curves, params, which)
    #params = [0.75, 1.2]
    data['cf_predicted'] = predict_power(trace_config, data, data_curves, params, which)
    
    result = {
        'trace': trace_name,
        'era5_hourly': rmse_timeseries(data),
        'ninja_hourly': rmse_timeseries(data, predictor='cf_ninja'),
        'era5_dcurve': rmse_duration(data),
        'ninja_dcurve': rmse_duration(data, predictor='cf_ninja'),
        #'rmse_durationseries': rmse_duration2(data), 
        'era5_daily': rmse_daily(data),
        'ninja_daily': rmse_daily(data, predictor='cf_ninja'),
        #'param_str': param_str,
    }
    result.update(param_dict(params, which))
    return result
    
results = []
#geometries = []
    
for trace_name, trace_config in progressbar.progressbar(config_traces.items()):
    #fig, axes = plt.subplots(1,3, figsize=(15, 5))
    #axes = list(axes.flatten())

    #if not trace_name.startswith('Barrow'):
    #    continue
    #print(trace_name)
    
    #plot_func = plot_duration_curve
    try:
        data, data_curves, data_farms = load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level)
    except Exception:
        break

    assert(len(trace_config['farms']) == 1)
    farm_name = trace_config['farms'][0]
    farm = data_farms.loc[farm_name]
    hub = farm['Hub height (m)']
    lat = farm['Latitude']
    lon = farm['Longitude']
    num_turbines = data_farms.loc[farm_name]['Number turbines']
        
    #results.append(get_result(data, data_curves, 'wakeloss', fit_duration_curve))
    #results[-1]['hub height'] = hub
    #results[-1]['num turbines'] = num_turbines
    #plot_func(data, axes.pop(0), f'{trace_name}\ntime series')
    
    results.append(get_result(data, data_curves, 'wakeloss sigma', fit_duration_curve))
    if results[-1]['sigma'] < 0:
        results[-1]['sigma'] = 0
    #results[-1]['hub height'] = hub
    #results[-1]['num turbines'] = num_turbines
    #results[-1]['lat'] = lat
    #results[-1]['lon'] = lon
    #geometries.append(Point(lon, lat))
    #plot_func(data, axes.pop(0), f'{trace_name}\nduration curve')
    
    #fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    #plot_duration_curve(data, ax=ax1, title=f'{farm_name}\nera5')
    #plot_duration_curve(data, ax=ax2, title='ninja', predictor='cf_ninja')
    
    
results = pd.DataFrame(results)
results.to_csv(f'output/ninja_comparison_optimised.csv', index=False)
#gdf = gpd.GeoDataFrame(results, geometry=geometries)
#gdf.to_file('output/results.shp')

print()
print(results.mean())
print()
print(results.std())

#plot_duration_curve(data)
#print(results[0])
