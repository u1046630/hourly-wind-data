#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020
@author: liam
"""

import pandas as pd, numpy as np
import progressbar
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from funcs_plot import plot_duration_curve
from funcs_data import load_all
from funcs_transform import fit, predict_power
from funcs_error import rmse_duration, rmse_timeseries
from funcs_util import setup, param_dict

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2010, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'

time_vect = np.arange(era5_date_start, era5_date_end, timedelta(hours=1))
df = pd.DataFrame(index=time_vect)
cf_predicted = pd.DataFrame(index=time_vect)
cf_actual = pd.DataFrame(index=time_vect)
capacity = pd.DataFrame(index=time_vect)

errors = []
    
for trace_name, trace_config in progressbar.progressbar(config_traces.items()):
    
    if trace_config['country'] != 'uk':
        continue
    
    data, data_curves = load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level)
    
    params, which = setup('wakeloss')
    #params, _ = fit(trace_config, data, data_curves, params, which)
    params[0] = 0.797
    data['cf_predicted'] = predict_power(trace_config, data, data_curves, params, which)
    
    errors.append(rmse_timeseries(data))
    
    cf_predicted[trace_name] = data['cf_predicted'] * data['capacity_actual']
    cf_actual[trace_name] = data['cf_actual'] * data['capacity_actual']
    capacity[trace_name] = data['capacity_actual']
    
df['cf_actual'] = cf_actual.sum(axis=1) / capacity.sum(axis=1)
df['cf_predicted'] = cf_predicted.sum(axis=1) / capacity.sum(axis=1)
df['num'] = (~np.isnan(cf_actual)).sum(axis=1)
df['effective_num'] = capacity.sum(axis=1)**2 / (capacity**2).sum(axis=1)

df = df.dropna(how='any')

print(np.mean(errors))
print(rmse_timeseries(df))
print(np.nanmean(df.effective_num))
