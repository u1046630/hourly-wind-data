#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020

@author: liam

two modes: 
1. area (generation trace is aggregated from many wind farms in area)
2. individual (generation trace is of a single farm)
"""

# TODO: 
# 1. double check all the "alternative_turbine" stuff, it could really make a difference
# 2. compute an error metric, apply same wakeloss to all traces and plot resulting error against wakeloss
# make a nice plot with shaded area within the standard dev


import pandas as pd, numpy as np
import progressbar
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from funcs_plot import plot_duration_curve
from funcs_data import load_all
from funcs_transform import predict_power
from funcs_util import setup
from funcs_error import rmse_timeseries, rmse_duration

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2010, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'
    
# data_entsoe: time indexed dataframe of hourly power trace from ENTSO-E
# data_era5: time indexed dataframe of hourly wind speeds from ERA5
# data_capacity: time index dataframe of hourly capacity (from thewindpower.net)

#name0 = 'London Array 1'
#sigma0 = 0.056717337900332
wakeloss0 = 0.279088019909239

# =============================================================================
# name0 = 'Belwind II'
# sigma0 = 1.45965721273292
# wakeloss0 = 0.765642634201729
# =============================================================================

name0 = 'Race Bank 1'
sigma0 = 2.06815028917832
#wakeloss0 = 1.23225680803559
    
vect_wakeloss = [wakeloss0] #np.linspace(wakeloss0 - 2, wakeloss0 + 2, num=41)
vect_sigma = [sigma0] #np.arange(1.0, 2.0, 0.1)
results = []
    
for trace_name, trace_config in progressbar.progressbar(config_traces.items()):
    
    if trace_name != name0:
        continue
    
    data, data_curves, _ = load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level)
    
    params, which = setup('wakeloss sigma')
    for wakeloss in vect_wakeloss:
        for sigma in vect_sigma:
            data['cf_predicted'] = predict_power(trace_config, data, data_curves, [wakeloss, sigma], which)
            error_dcurve = rmse_duration(data)
            error_timeseries = rmse_timeseries(data)
            results.append({
                'trace': trace_name,
                'wakeloss': wakeloss,
                'sigma': sigma,
                'rmse_timeseries': error_timeseries,
                'rmse_durationcurve': error_dcurve,
            })
    
    
results = pd.DataFrame(results)
#results.to_csv(f'output/sensitivity-{name0}.csv', index=False)



#%%

plot_duration_curve(data, title=f'{name0}\nwakeloss={wakeloss0:.2f}')

# =============================================================================
# plt.plot(x0, y0, color='blue')
# plt.plot(x2, y2, color='orange')
# plt.scatter([x0[y0.argmin()]], [y0.min()], color='blue')
# plt.scatter([x2[y2.argmin()]], [y2.min()], color='orange')
# plt.legend(['London Array 1', 'Race Bank 1'])
# plt.xlabel('wakeloss')
# plt.ylabel('RMSE (duration curve)')
# plt.title('Accuracy of predicted duration curve')
# =============================================================================

