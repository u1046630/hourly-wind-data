#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:31:11 2020

@author: liam
"""

import numpy as np

def effective_wind_speed(wind, temperature):
    # values from https://en.wikipedia.org/wiki/Density_of_air
    temp_vect = np.array([35,30,25,20,15,10,5,0,-5,-10,-15,-20,-25])[::-1] + 273.15 # Kelvin
    density_vect = np.array([1.1455,1.1644,1.1839,1.2041,1.225,1.2466,1.269,1.2922,1.3163,1.3413,1.3673,1.3943,1.4224])[::-1]
    rel_density = np.interp(temperature, temp_vect, density_vect)
    rel_density /= rel_density.mean()
    wind_power = wind**3 * rel_density
    return wind_power**(1/3)

def setup(param_str):
    param_str = param_str.split(' ')
    params = []
    which = []
    if 'wakeloss' in param_str:
        params.append(1.0)
        which.append(0)
    if 'sigma' in param_str:
        params.append(2.0)
        which.append(1)
    if 'k2' in param_str:
        params.append(1.0)
        which.append(2)
    return params, which

def param_dict(params, which):
    x = {}
    which_dict = {0: 'wakeloss', 1: 'sigma', 2: 'k2'}
    for k, v in zip(which, params):
        x[which_dict[k]] = v
    if 'wakeloss' not in x:
        x['wakeloss'] = 0.0
    if 'sigma' not in x:
        x['sigma'] = 0.0
    if 'k2' not in x:
        x['k2'] = 1.0
    return x