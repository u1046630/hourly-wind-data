#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 18:51:49 2020

@author: liam
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import glob
from collections import OrderedDict

def load_df(input_pattern):
    dfs = [pd.read_csv(fname) for fname in glob.glob(input_pattern)]
    return pd.concat(dfs)

def filter_df(df, wakeloss=None, sigma=None):
    if wakeloss:
        df = df[df.wakeloss == wakeloss]
    if sigma:
        df = df[df.sigma == sigma]
    return df

def plot(df, ax, param, wakeloss=None, sigma=None):
    if wakeloss is None or sigma is None:
        raise Exception('[!] must define all parameters')
    
    params = OrderedDict([
        ('wakeloss', wakeloss),
        ('sigma', sigma),
    ])
    param_list = list(params.keys())
    indexer = params.copy()
    indexer[param] = slice(None)
    indexer = tuple(indexer.values())
    
    mean = df.groupby(by=param_list).mean().loc(axis=0)[indexer]
    std = df.groupby(by=param_list).std().loc(axis=0)[indexer]
    
    i = list(params.keys()).index(param)
    x_vect = [x[i] for x in mean.index]
    
    ax.plot(x_vect, mean.error, 'black')
    ax.fill_between(x_vect, mean.error + std.error, mean.error - std.error, color='lightgrey')
    x_min = mean[mean.error == mean.min()[0]].index[0]
    
    indexer = tuple(params.values())
    vert_mean, vert_std = mean.error.loc[indexer], std.error.loc[indexer]
    ax.plot([x_min[i], x_min[i]], [vert_mean - vert_std, vert_mean + vert_std], color='green')
    ax.set_ylabel('RMSE')
    ax.set_title(f'{param}={params[param]:.2f}')
    ax.set_xlabel(param)
    ax.set_ylim([0.0,0.12])
    
weights = pd.read_csv('weights.csv').set_index('trace')
weights.weight /= weights.weight.sum()
    
df = load_df('output/actual-hub-height-sensitivity-analysis.csv')
df = df.rename(columns={'rmse_durationcurve': 'error'})
df = df.drop(columns=['rmse_timeseries'])

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))

if False:
    for i, row in df.iterrows():
        df.at[i, 'error2'] = df.at[i, 'error'] * weights.loc[row.trace].weight
    df = df.drop(columns=['error']).rename(columns={'error2': 'error'})
    mean = df.groupby(by=['wakeloss', 'sigma']).sum()
else:
    mean = df.groupby(by=['wakeloss', 'sigma']).mean()
# filter:
#mean = mean.loc(axis=0)[slice(None), slice(None), 1.0]
    
mean.loc[(0.75, 1.2)] = 0.04
    
min_err = mean.min()[0]
wakeloss, sigma = mean[mean.error == min_err].index[0]

mean
plot(df, ax1, 'wakeloss', wakeloss=wakeloss, sigma=sigma)
plot(df, ax2, 'sigma', wakeloss=wakeloss, sigma=sigma)



