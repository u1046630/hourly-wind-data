#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:36:00 2020

@author: liam
"""

import pandas as pd
import numpy as np
from scipy import ndimage
from scipy import optimize

from wind.drives import thewindpower_dir

dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'

def fit_timeseries(trace_config, data, data_curves, p0, which):
    #df = data.groupby(pd.Grouper(freq='D')).mean()
    #data = data.dropna(how='any')
    
    def func(dummy, *params):
        cf_predicted = predict_power(trace_config, data, data_curves, params, which)
        return cf_predicted
    
    params, pcov = optimize.curve_fit(func, None, data.cf_actual, p0=p0)
    perr = np.sqrt(np.diag(pcov))
    
    return params, perr

def fit_duration_curve(trace_config, data, data_curves, p0, which):
    dcurve = np.array(data.cf_actual)
    dcurve.sort()
    
    def func(dummy, *params):
        cf_predicted = np.array(predict_power(trace_config, data, data_curves, params, which))
        cf_predicted.sort()
        return cf_predicted
    
    params, pcov = optimize.curve_fit(func, None, dcurve, p0=p0)
    perr = np.sqrt(np.diag(pcov))
    
    return params, perr

def minimise_duration_errors(trace_config, data, data_curves, p0, which):
    dcurve_y = np.array(data.cf_actual)
    dcurve_y.sort()
    dcurve_x = np.linspace(0, 1, num=len(dcurve_y))
    
    duration_actual = np.interp(data.cf_actual, dcurve_y, dcurve_x)
    
    def func(dummy, *params):
        cf_predicted = predict_power(trace_config, data, data_curves, params, which)
        duration_predicted = np.interp(cf_predicted, dcurve_y, dcurve_x)
        return duration_predicted
    
    params, pcov = optimize.curve_fit(func, None, duration_actual, p0=p0)
    perr = np.sqrt(np.diag(pcov))
    
    return params, perr
    

def adjust_power_curve(curve_x, curve_y, params, which):
    defaults = [0.0, 0.00001, 1.0] # wake_loss, sigma, k2
    for index, param in zip(which, params):
        defaults[index] = param
    wakeloss,sigma,k2 = defaults
    
    x_prime = curve_x.copy()
    y_prime = curve_y.copy()
    
    x_prime = wakeloss + x_prime * k2
    if sigma > 0:
        step = x_prime[1] - x_prime[0]
        sigma /= step
        y_prime = ndimage.filters.gaussian_filter1d(y_prime, sigma, mode='nearest')
    
    return x_prime, y_prime
    
def predict_power(trace_config, data, curve_data, params, which):
    
    predicted = pd.DataFrame(data.index, columns=['DateTime']).set_index('DateTime')
    predicted['power_predicted'] = np.zeros(len(predicted), dtype='float')
    predicted['capacity_predicted'] = np.zeros(len(predicted), dtype='float')
    
    for name in trace_config['farms']:
        curve_x, curve_y = adjust_power_curve(*curve_data[name], params, which)
        wind = data[f'wind_{name}']
        capacity = data[f'capacity_{name}']
        predicted.power_predicted += capacity * np.interp(wind, curve_x, curve_y)
        predicted.capacity_predicted += capacity
        
    predicted['cf_predicted'] = predicted['power_predicted'] / predicted['capacity_predicted']
    
    return predicted['cf_predicted']