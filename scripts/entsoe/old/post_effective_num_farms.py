#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 14:23:32 2020

@author: liam
"""
import pandas as pd, numpy as np, glob

fnames = glob.glob('/home/liam/Downloads/thewindpower-countries/*')

for fname in fnames:
    print(fname.split('/')[-1].split('.')[0])
    
    if fname.endswith('html'):
        x = pd.read_html(fname, header=0)[2]
        x = x[x.index % 2 == 1]
        x = x.rename(columns={'Power (kW)': 'Power'})
        x = x[x.Power != 'ND']
        x = x.astype({'Power': 'float'})
    elif fname.endswith('csv'):
        x = pd.read_csv(fname, names=['Power'])
        
    power = np.array(x.Power)
    
    print(f'  Capacity: {power.sum() / 1000:.2f}')
    print(f'  Average: {power.mean() / 1000:.2f}')
    print(f'  Std:     {power.std() / 1000:.2f}')
    print(f'  Number: {len(x)}')
    factor = power.sum() / np.sqrt(np.sum(power**2))
    print('  Error reduction factor: {:.1f}'.format(factor))
    print('  Effective # farms: {:.0f}'.format(factor**2))
    print()
