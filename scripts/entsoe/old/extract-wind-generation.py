#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 14:27:16 2020

@author: liam
"""

import pandas as pd, numpy as np
import glob, progressbar
from datetime import datetime, timedelta
from calendar import monthrange

from wind.drives import entsoe_dir

# only two offshore wind farms change their capacity during the period
# Dec 2014 to present, '48W00000LNCSO-2P' and '48W00000LNCSO-1R'

input_dir = entsoe_dir + 'sftp/'
output_dir = entsoe_dir + 'sftp-processed/'

data = {}
metadata = {}

dtypes = {
    'DateTime': 'str',
    'AreaCode': 'str',
    'AreaTypeCode': 'str',
    'AreaName': 'str',
    'MapCode': 'str',
    'GenerationUnitEIC': 'str',
    'PowerSystemResourceName': 'str',
    'ProductionTypeName': 'str',
    'ActualGenerationOutput': 'float',
    'InstalledGenCapacity': 'float',
}

fnames = glob.glob(input_dir + '*')
for fname in progressbar.progressbar(fnames):
    data = pd.read_csv(fname, encoding='utf-16', delimiter='\t', 
                       parse_dates=['DateTime'], dtype=dtypes, usecols=dtypes.keys())
    data = data[data.ProductionTypeName == 'Wind Offshore']
    eics = set(data.GenerationUnitEIC)
    
    for eic in eics:
        subdata = data[data.GenerationUnitEIC == eic]
        
        # capture metadata
        row = subdata.iloc[0]
        entry = {
            'AreaCode': row.AreaCode,
            'AreaTypeCode': row.AreaTypeCode,
            'AreaName': row.AreaName,
            'MapCode': row.MapCode,   
            'PowerSystemResourceName': row.PowerSystemResourceName,   
            'ProductionTypeName': row.ProductionTypeName,
        }
        if eic in metadata:
            assert(metadata[eic] == entry)
        else:
            metadata[eic] = entry
            data[eic] = []
            
        # capture generation and installed
        subdata = subdata[['DateTime','ActualGenerationOutput','InstalledGenCapacity']]
        subdata.set_index('DateTime', inplace=True)
        subdata = subdata.groupby(pd.Grouper(freq='H')).mean() # get hourly m
        subdata = subdata.dropna(how='any')
        data[eic].append(subdata)
        break
    
for eic, frames in data.items():
    temp = pd.concat(frames)
    np.savez(output_dir + eic + '.npz', time=temp.index, **temp)
    
## TODO: save metadata    
        
    
