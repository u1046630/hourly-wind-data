
# API documentation:
# https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html
# Section 4.4.7 is "Actual Generation Output per Generation Unit [16.1.A]"
# 

import requests, os
import numpy as np
from xml.etree import ElementTree
from datetime import datetime, timedelta
import threading, time

from wind.drives import entsoe_dir

###############
### globals ###
###############

country = 'denmark'
generation_type = 'offshore wind'
output_dir = entsoe_dir + 'offshore-denmark-raw/'
date_start = datetime(2014, 1, 1)
date_end = datetime(2020, 1, 1)
request_rate = 100 # requests per minute (limit is 400)

url = 'https://transparency.entsoe.eu/api'

#############
### codes ###
#############

params = {
    'securityToken': 'dd44125e-35d0-442f-b638-c3d4c7f06579',
    'documentType': 'A73', # actual generation
    'processType': 'A16', # realised (as opposed to day ahead for example)
    'in_Domain': None, # use a control_area_codes
    'periodStart': '201912312300', # YYYYMMDDhhmm format
    'periodEnd': '202001012300', # YYYYMMDDhhmm format
    'psrType': None, # resource type, use a type_code (optional)
    'RegisteredResource': None, # to select a particular generator (optional)
}
type_codes = { # API docs section A.5
    'onshore wind': 'B19',
    'offshore wind': 'B18',
}
control_area_codes = { # API docs section A.10
    'denmark': '10Y1001A1001A796', # Energinet
}

#############
### funcs ###
#############

def get_clean_xml(url, params):
    response = requests.get(url, params=params)
    if response.status_code != requests.codes.ok:
        return None, (response.status_code, response.content)
    root = ElementTree.fromstring(response.content)
    for elem in root.getiterator():
        elem.tag = elem.tag.split('}')[-1]
    return root, None

def download_data(url, params, fname, date):
    
    #print('   > {}: started'.format(date))
    results = {}
    
    params['periodStart'] = datetime.strftime(date, '%Y%m%d%H%M')
    params['periodEnd'] = datetime.strftime(date + timedelta(days=1), '%Y%m%d%H%M')
    root, error = get_clean_xml(url, params)
    
    if error is not None:
        code, content = error
        if code == 429:
            print('   > {}: BEING RATE LIMITED!'.format(date))
            return
        elif code == 500:
            print('   > {}: internal server error'.format(date))
            return
        elif b'No matching data found' in content:
            print('   > {}: no data found'.format(date))
        elif b'is not valid for this area' in content:
            print('   > {}: no installed generation in this period'.format(date))
        else:
            print('   > {}: other HTTP error code={}\n"{}"'.format(date, code, content))
            return
        
        np.savez(fname) # save a blank file to indicate no data
        return
    
    for x in root.findall('.//TimeSeries'):
    
        name = x.find('./MktPSRType/PowerSystemResources/name').text
        assert(name not in results) # names should be unique
        results[name] = np.full(24, np.nan)
            
        resolution = x.find('./Period/resolution').text
        assert(resolution == 'PT60M') # hourly time resolution
        
        # each timeseries has it's own datetime start value
        # which differs if first few data points are missing
        period_start = x.find('./Period/timeInterval/start').text
        period_start = datetime.strptime(period_start, '%Y-%m-%dT%H:%MZ')
        
        pts = x.findall('./Period/Point')
        for pt in pts:
            pos = int(pt.find('./position').text) # 1-based index from period_start
            value = int(pt.find('./quantity').text)
            results[name][pos - 1] = value
        
    np.savez(fname, **results)
    print('   > {}: done'.format(date))
    

############
### main ###
############
    
assert(os.path.isdir(output_dir))

params['in_Domain'] = control_area_codes[country]
params['psrType'] = type_codes[generation_type]

ndays = (date_end - date_start) // timedelta(days=1)

threads = []

print('[*] starting threads at rate of {} per minute...'.format(request_rate))

for iday in range(ndays)[::-1]:
    date = date_start + timedelta(days=iday)
    fname = output_dir + datetime.strftime(date, '%Y-%m-%d') + '.npz'
    if os.path.exists(fname):
        #print('   > {}: file already downloaded'.format(date))
        continue
    
    thread = threading.Thread(target=download_data, args=(
        url, params.copy(), fname, date
    ))
    thread.start()
    threads.append(thread)
    time.sleep(60 / request_rate)
    
print('[*] all threads started. waiting for them to complete...')
    
for thread in threads:
    thread.join()
    
print('[*] done!')

