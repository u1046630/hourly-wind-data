#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 15:46:18 2020

@author: liam
"""

import numpy as np, os
from datetime import datetime, timedelta

from wind.drives import entsoe_dir

input_dir = entsoe_dir + 'offshore-denmark-raw/'
output_dir = entsoe_dir + 'offshore-denmark-processed/'
date_start = datetime(2014, 1, 1)
date_end = datetime(2020, 1, 1)

ndays = (date_end - date_start) // timedelta(days=1)

results = {}

for iday in range(ndays):
    date = date_start + timedelta(days=iday)
    fname = '{}{}.npz'.format(input_dir, datetime.strftime(date, '%Y-%m-%d'))
    if not os.path.exists(fname):
        raise Exception('file does not exist: {}'.format(fname))
    npz = np.load(fname)
    for name, array in npz.items():
        if name not in results:
            results[name] = np.full(ndays*24, np.nan)
        results[name][iday*24:(iday+1)*24] = array[:]
        
for name, array in results.items():
    fname = output_dir + name + '.npy'
    np.save(fname, array)
    
time_array = np.arange(date_start, date_end, timedelta(hours=1))
np.save(output_dir + 'time.npy', time_array)
    