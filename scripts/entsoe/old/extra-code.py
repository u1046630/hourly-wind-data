#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 29 11:59:16 2020

@author: liam
"""

# Extra unused code

# load and aggreate acutal power and capacity from several ENTSO-E codes
def get_actual_power1(data_dir, codes):
    dfs = []
    n = len(codes)
    cols_power = [f'ActualGenerationOutput{i}' for i in range(n)]
    cols_cap = [f'InstalledGenCapacity{i}' for i in range(n)]
    
    for i, (code, corrections) in enumerate(codes.items()):
        # ignore corrections for now
        df = df_from_npz(f'{data_dir}{code}.npz')
        df = df.add_suffix(f'{i}')
        dfs.append(df)
    df = pd.DataFrame().join(dfs, how='outer').dropna(how='any')
    
    return pd.DataFrame.from_dict({
        'power_actual': df[cols_power].sum(axis=1),
        'capacity_published': df[cols_cap].sum(axis=1),        
    })
    
    
    
    
    
    
    
    
    
    
    
    

############
### main ###
############
    
results = []

for trace_name, trace_config in config_traces.items():
    
    if not trace_name.startswith('Burbo Bank 1'):
        continue
    #if trace_config['country'] != 'uk':
    #    continue
    print(trace_name)
    
    # data_entsoe: time indexed dataframe of hourly power trace from ENTSO-E
    # data_era5: time indexed dataframe of hourly wind speeds from ERA5
    # data_capacity: time index dataframe of hourly capacity (from thewindpower.net)

    data_entsoe = load_entsoe(trace_config, traces_dirs)
    
    data_entsoe = data_entsoe[(data_entsoe.index >= era5_date_start) & (data_entsoe.index < era5_date_end)]
    date_start = data_entsoe.index[0]
    date_end = data_entsoe.index[-1] + timedelta(hours=1)
    
    data_farms = load_countryinfo(trace_config)
    
    data_capacity, data_curves = load_thewindpower(trace_config, data_farms, date_start, date_end)
    data_era5 = load_era5(trace_config, data_farms, date_start, date_end)
    
    data = data_entsoe.join([data_era5, data_capacity])
    del data_entsoe, data_era5, data_capacity
    
    params, which = (), ()
    data['cf_fit0'] = predict_power(trace_config, data, data_curves, params, which)
    
    params, which = setup('wakeloss')
    params, err = fit(trace_config, data, data_curves, params, which)
    params = list(params) + [0.00001]
    which.append(1)
    data['cf_fit1'] = predict_power(trace_config, data, data_curves, params, which)
    
    params, which = setup('wakeloss')
    params, err = fit(trace_config, data, data_curves, params, which)
    params = list(params) + [1]
    which.append(1)
    data['cf_fit2'] = predict_power(trace_config, data, data_curves, params, which)
    
    params, which = setup('wakeloss')
    params, err = fit(trace_config, data, data_curves, params, which)
    params = list(params) + [2]
    which.append(1)
    data['cf_fit3'] = predict_power(trace_config, data, data_curves, params, which)
    #show(params, which)
    
    results.append({'name': trace_name, 'wakeloss': params[0], 'wakeloss_std': err[0], 'capacity': data.capacity_actual.max()})
    
# =============================================================================
#     params, which = setup('sigma')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit2'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
#     
#     params, which = setup('k2')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit3'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
#     
#     params, which = setup('wakeloss k2')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit4'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
#     
#     params, which = setup('wakeloss sigma')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit5'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
#     
#     params, which = setup('k2 sigma')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit6'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
#     
#     params, which = setup('wakeloss sigma k2')
#     params, err = fit(trace_config, data, data_curves, params, which)
#     data['cf_fit7'] = predict_power(trace_config, data, data_curves, params, which)
#     show(params, which)
# =============================================================================
    
    
    ############
    ### plot ###
    ############
    
# =============================================================================
    fig, (ax3) = plt.subplots(1, 1, figsize=(5,5))
# =============================================================================
    
# =============================================================================
#     data.plot.scatter(x='cf_fit1', y='cf_actual', s=0.2, alpha=0.2, ax=ax1, color='purple')
#     ax1.plot([0, 1], [0, 1], color='black')
#     ax1.set_title(trace_name + '\nno adjustments')
#     
#     data.plot.scatter(x='cf_fit3', y='cf_actual', s=0.2, alpha=0.2, ax=ax2, color='brown')
#     ax2.plot([0, 1], [0, 1], color='black')
#     ax2.set_title(trace_name + f'\nwake_loss={params[0]:.2f}m/s')
# =============================================================================
    
    #data.plot.scatter(x='cf_fit2', y='cf_actual', s=0.2, alpha=0.2, ax=ax3, color='green')
    #ax3.plot([0, 1], [0, 1], color='black')
    #ax3.set_title(trace_name + f'\nwake_loss={params2[0]:.2f}m/s, sigma={params2[1]:.2f}m/s')
    
# =============================================================================
    plot_residue_diag(data, 'cf_fit0', 'cf_actual', ax3, color='blue')
    plot_residue_diag(data, 'cf_fit1', 'cf_actual', ax3, color='purple')
    plot_residue_diag(data, 'cf_fit2', 'cf_actual', ax3, color='green')
    plot_residue_diag(data, 'cf_fit3', 'cf_actual', ax3, color='brown')
#     plot_residue_diag(data, 'cf_fit4', 'cf_actual', ax3)
#     plot_residue_diag(data, 'cf_fit5', 'cf_actual', ax3)
#     plot_residue_diag(data, 'cf_fit6', 'cf_actual', ax3)
#     plot_residue_diag(data, 'cf_fit7', 'cf_actual', ax3)
#     #plot_residue(data, 'cf_fit3', 'cf_actual', ax3)
    ax3.plot([0,1],[0,0], color='black')
#    ax3.legend([f'fit{i}' for i in  range(8)])
    ax3.set_title(f'{trace_name}\nresidues')
# =============================================================================
    
results = pd.DataFrame(results)
results.to_csv('output.csv', index=False)