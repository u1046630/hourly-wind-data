#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 08:51:46 2020

@author: liam
"""

import pandas as pd, numpy as np
import os, glob
import matplotlib.pyplot as plt

from wind.drives import entsoe_dir

fname = entsoe_dir + 'eic-codes/W_eiccodes.csv'
my_dir = entsoe_dir + 'sftp-processed/*'

df = pd.read_csv(fname, index_col=0)
df.rename(columns={x: x.strip() for x in df.columns}, inplace=True)

for x in glob.glob(my_dir):
    basename = os.path.basename(x).split('.npy')[0]
    
    if basename == 'time':
        continue
    
    try:
        print('{} ==> {}'.format(basename, df.loc[basename].EicLongName))
    except KeyError:
        print('{} ==> Not found'.format(basename))
        
        
# what wind farm is '48W00000WLNYW-1A'?
# doesn't appear in EIC-codes
# from sftp/*.csv it has:
# GenerationUnitEIC == '48W00000WLNYW-1A' (its Walney in UK)
# PowerSystemResourceName == WLNYW-1
# Don't forget InstalledGenGapacity

# Horns Rev C generation unit
        # 45W000000000116N
        # Horns Rev C