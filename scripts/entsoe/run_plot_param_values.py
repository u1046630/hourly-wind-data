#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 10:41:57 2020
@author: liam
"""

import pandas as pd, numpy as np
import progressbar
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from funcs_plot import plot_duration_curve, plot_residue_diag, plot_error_duration, plot_duration_curve3, plot_daily_cf, plot_hourly_cf, plot_step_changes
from funcs_data import load_all
from funcs_transform import fit_timeseries, fit_duration_curve, minimise_duration_errors, predict_power
from funcs_error import rmse_duration, rmse_timeseries, rmse_duration2, rmse_daily
from funcs_util import setup, param_dict

from wind.drives import thewindpower_dir

from config import traces_dirs, config_traces

# extent of the ERA5 data avaliable:
era5_date_start = datetime(2014, 1, 1)
era5_date_end = datetime(2020, 1, 1)
level = 133 # 100m
dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'
    
for trace_name, trace_config in progressbar.progressbar(config_traces.items()):
    
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(20, 5))
    
    data, data_curves, data_farms = load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level)
    
    params, which = setup('wakeloss sigma')
    params = 0.75, 1.2
    #params, _ = fit_duration_curve(trace_config, data, data_curves, params, which)
    data['cf_predicted'] = predict_power(trace_config, data, data_curves, params, which)
    
    title1 = f'{trace_name}\nwakeloss={params[0]:.2f}, sigma={params[1]:.2f}'
    title2 = 'RMSE={:.1f}%'.format(rmse_timeseries(data)*100)
    title3 = 'RMSE={:.1f}%'.format(rmse_daily(data)*100)
    title4 = 'RMSE={:.1f}%'.format(rmse_duration(data)*100)
    
    plot_step_changes(data, ax=ax1, title=title1)
    plot_hourly_cf(data, ax=ax2, title=title2)
    plot_daily_cf(data, ax=ax3, title=title3)
    plot_duration_curve(data, ax=ax4, title=title4)
    
    