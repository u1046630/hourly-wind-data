#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 13:45:47 2020

@author: liam
"""

import pandas as pd
import numpy as np
import glob, progressbar, os

from wind.drives import entsoe_dir

input_dir = entsoe_dir + 'InstalledGenerationCapacityAggregated-raw/'
output_dir = entsoe_dir + 'InstalledGenerationCapacityAggregated/'

dtypes = {
    'areacode': 'str', # UID
    'AreaName': 'str', # metadata
    'MapCode': 'str', # metadata
    'ProductionType': 'str', # for filtering
    'AggregatedInstalledCapacity': 'float', # data
    'DateTime': 'str', # data
}

metadata = {}
data = {}

fnames = glob.glob(input_dir + '*.csv')
for i,fname in progressbar.progressbar(list(enumerate(fnames))):
    
    df = pd.read_csv(fname, encoding='utf-16', delimiter='\t', 
                       dtype=dtypes, usecols=dtypes.keys(), parse_dates=['DateTime'])
    df = df[df.ProductionType == 'Wind Offshore']
    
    codes = set(df.areacode)
    for code in codes:
        row = df[df.areacode == code].iloc[0]
        if code not in metadata:
            data[code] = []
            metadata[code] = {
                'AreaCode': row.areacode,
                'AreaName': set([row.AreaName]), # there's often multiple AreaNames
                'MapCode': set([row.MapCode]),
            }
        else:
            metadata[code]['AreaName'].add(row.AreaName)
            metadata[code]['MapCode'].add(row.MapCode)
        
        # sub dataframe, just the bits I want to extract
        subdf = df[df.areacode == code][['DateTime','AggregatedInstalledCapacity']]
        subdf.set_index('DateTime', inplace=True)
        data[code].append(subdf)
        
if not os.path.exists(output_dir):
    os.mkdir(output_dir)
        
for code, dfs in data.items():
    df = pd.concat(dfs)
    df.sort_index(inplace=True)
    
    temp = metadata[code]
    temp['AreaName'] = '/'.join(temp['AreaName'])
    temp['MapCode'] = '/'.join(temp['MapCode'])
    temp['NumPoints'] = len(df)
    temp['DateStart'] = df.index.min()
    temp['DateEnd'] = df.index.max()
    
    fname = '{}{}.npz'.format(output_dir, code)
    np.savez(fname, DateTime=df.index, **df)
    
metadata = pd.DataFrame(metadata.values())
metadata.to_csv(output_dir + 'metadata.csv', index=False)

    
    