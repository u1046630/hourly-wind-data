#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 19 13:45:47 2020

@author: liam
"""

import pandas as pd
import numpy as np
import glob, progressbar, os

from wind.drives import entsoe_dir

input_dir = entsoe_dir + 'ActualGenerationOutputPerUnit-raw/'
output_dir = entsoe_dir + 'ActualGenerationOutputPerUnit/'

dtypes = {
    'DateTime': 'str', # data
    'AreaCode': 'str', # metadata
    'AreaTypeCode': 'str', # metadata
    'AreaName': 'str', # metadata
    'MapCode': 'str', # metadata
    'GenerationUnitEIC': 'str', # UID
    'ProductionTypeName': 'str', # metadata
    'PowerSystemResourceName': 'str', # for filtering
    'ActualGenerationOutput': 'float', # data
    'InstalledGenCapacity': 'float', # data
}
# these attributes should be collected into the metadata.csv
metadata_collect = ('AreaCode','AreaTypeCode','AreaName','MapCode','PowerSystemResourceName')

metadata = {}
data = {}

fnames = glob.glob(input_dir + '*.csv')
for i,fname in progressbar.progressbar(list(enumerate(fnames))):
    
    df = pd.read_csv(fname, encoding='utf-16', delimiter='\t', 
                       dtype=dtypes, usecols=dtypes.keys(), parse_dates=['DateTime'])
    df = df[df.ProductionTypeName == 'Wind Offshore']
    
    codes = set(df.GenerationUnitEIC)
    for code in codes:
        row = df[df.GenerationUnitEIC == code].iloc[0]
        if code not in metadata:
            data[code] = []
            metadata[code] = {'GenerationUnitEIC': row.GenerationUnitEIC}
            for key in metadata_collect:
                metadata[code][key] = set([row[key]])
        else:
            for key in metadata_collect:
                metadata[code][key].add(row.AreaCode)
        
        # sub dataframe, just the bits I want to extract
        subdf = df[df.GenerationUnitEIC == code][['DateTime','ActualGenerationOutput', 'InstalledGenCapacity']]
        subdf.set_index('DateTime', inplace=True)
        subdf = subdf.groupby(pd.Grouper(freq='H')).mean() # get hourly mean
        subdf = subdf.dropna(how='all')
        data[code].append(subdf)
        
if not os.path.exists(output_dir):
    os.mkdir(output_dir)
        
for code, dfs in data.items():
    df = pd.concat(dfs)
    df.sort_index(inplace=True)
    
    temp = metadata[code]
    for key in metadata_collect:
        temp[key] = '/'.join(temp[key])
    temp['NumPoints'] = len(df)
    temp['OutputMin'] = df.ActualGenerationOutput.min()
    temp['OutputMax'] = df.ActualGenerationOutput.max()
    temp['CapacityMin'] = df.InstalledGenCapacity.min()
    temp['CapacityMax'] = df.InstalledGenCapacity.max()
    temp['DateStart'] = df.index.min()
    temp['DateEnd'] = df.index.max()
    
    fname = '{}{}.npz'.format(output_dir, code)
    np.savez(fname, DateTime=df.index, **df)
    
metadata = pd.DataFrame(metadata.values())
metadata.to_csv(output_dir + 'metadata.csv', index=False)

    
    