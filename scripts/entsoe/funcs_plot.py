#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 07:57:03 2020

@author: liam
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

def plot_duration_curve(data, predictor='cf_predicted', ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    #data = data.groupby(pd.Grouper(freq='D')).mean()
    
    y1 = np.array(data.cf_actual)
    y2 = np.array(data[predictor])
    y1[::-1].sort()
    y2[::-1].sort()
    x = np.linspace(0, 1, len(y1))
    
    ax.plot(x, y1, color='blue')
    ax.plot(x, y2, color='orange')
    ax.legend(['Measurements', 'Model'])
    #ax.set_xlabel('Exceedance probability')
    ax.set_ylabel('Capacity Factor')
    ax.set_title(title)
    
    return ax

def plot_duration_curve3(data, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
    
    bins = 20
    df = data[['cf_actual', 'cf_predicted']].sort_values('cf_actual')
    df['duration'] = np.linspace(0, 1, len(df))
    grouper = df.groupby(np.linspace(0, bins, len(df))//1)
    x_vect = grouper.mean().duration[::-1]
    mean = grouper.mean().cf_predicted
    std = grouper.std().cf_predicted
    
    ax.plot(df.duration[::-1], df.cf_actual, color='blue')
    ax.plot(x_vect, mean, color='orange')
    ax.plot(x_vect, mean+std, color='orange')
    ax.plot(x_vect, mean-std, color='orange')
    ax.set_title(title)
    
def plot_duration_curve2(data, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    y = np.array(data.cf_actual)
    x = np.array(data.cf_predicted)
    y.sort()
    x.sort()
    
    ax.plot(x, y, color='blue')
    ax.plot([0, 1], [0, 1], color='orange')
    ax.legend(['curve', 'straight'])
    ax.set_xlabel('CF predicted')
    ax.set_ylabel('CF actual')
    ax.set_title(title)
    
def plot_step_changes(df, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    y1,x1 = np.histogram(np.diff(df.cf_actual), bins=200)
    x1 = (x1[1:] + x1[:-1]) / 2
    y2,x2 = np.histogram(np.diff(df.cf_predicted), bins=200)
    x2 = (x2[1:] + x2[:-1]) / 2
          
    ax.plot(x1, y1, color='blue')
    ax.plot(x2, y2, color='orange')
    ax.set_yscale('log')
    ax.legend(['Measurements', 'Model (ERA5)'])
    ax.set_title(title)
    
def plot_grouped_cf(df, ax=None, title='', freq='D', **kwargs):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    df = df.groupby(by=pd.Grouper(freq=freq)).mean()
    
    ax.scatter(df.cf_predicted, df.cf_actual, color='blue', **kwargs)
    x_min, x_max = df.cf_predicted.min(), df.cf_predicted.max()
    ax.plot([x_min, x_max], [x_min, x_max], color='orange')
    ax.set_xlabel('ERA5')
    ax.set_ylabel('Measurements')
    ax.set_title(title)
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    
def plot_error_duration(df, ax=None, title='', **kwargs):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    err = np.array(df.cf_actual - df.cf_predicted)
    err.sort()
    x = np.linspace(0, 1, len(err))
    ax.plot(x, err, **kwargs)
    ax.plot([0, 1], [0, 0], color='black')
    ax.set_title(title)
    
def plot_hourly_cf(df, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    plot_grouped_cf(df, ax, title, 'H', s=0.2, alpha=0.2)
    
def plot_daily_cf(df, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    plot_grouped_cf(df, ax, title, 'D', s=0.5, alpha=0.6)
    
def plot_monthly_cf(df, ax=None, title=''):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    plot_grouped_cf(df, ax, title, 'M', s=5)
    
def plot_residue(df, x, y, ax=None, **kwargs):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    radius = len(df) // 30
    temp = df[[x, y]].sort_values(x).rolling(radius*4, win_type='gaussian', center=True, min_periods=1).mean(std=radius)
    plt.plot(temp[x], temp[y] - temp[x], **kwargs)
    
def plot_residue_diag(df, ax=None, title='', **kwargs):
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(5,5))
        
    x = 'cf_actual'
    y = 'cf_predicted'
        
    radius = len(df) // 30
    temp = pd.DataFrame({'x': (df[y] + df[x]) / 2, 'y': df[y] - df[x]})
    temp = temp.sort_values('x').rolling(radius*4, win_type='gaussian', center=True, min_periods=1).mean(std=radius)
    ax.plot(temp.x, temp.y, **kwargs)
    ax.set_title(title)
    