#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:26:11 2020

@author: liam
"""

import pandas as pd
import numpy as np
import os
from datetime import datetime, timedelta
from scipy import ndimage
from scipy import optimize

from wind.drives import thewindpower_dir, ninja_dir
from wind.access_era5 import get_era5_data, wind_speed_at_hub_height

from config import alternative_turbines_new as alternative_turbines
from config import corrections

dir_power_curves = thewindpower_dir + 'turbines/power-curve-data/'

def parse_commission_date(x):
    # '2015' -> datetime(2015, 1, 1)
    # '2015/04' -> datetime(2015, 4, 1)
    if x is None or (isinstance(x, float) and np.isnan(x)):
        return None
    if '/' in x:
        y, m = x.split('/')
        return datetime(int(y), int(m), 1)
    return datetime(int(x),1,1)

def load_power_curve(turbine):
    
    if turbine in alternative_turbines:
        turbine = alternative_turbines[turbine]
    
    fname = dir_power_curves + turbine + '.csv'
    if not os.path.exists(fname):
        raise Exception('[!] power curve for {} doesnt exist'.format(turbine))
    power_curve = pd.read_csv(fname)
        
    curve_x = np.array(power_curve['Wind speed (m/s)'])
    curve_y = np.array(power_curve['Power (kW)'])
    
    return curve_x, curve_y
    
def load_entsoe(trace_config, dirs):
    fname = dirs[trace_config['type']] + trace_config['code'] + '.npz'
    npz = np.load(fname)
    data_entsoe = pd.DataFrame.from_dict({
        'DateTime': npz['DateTime'], 
        'power_actual': npz['ActualGenerationOutput'],
    }).set_index('DateTime').sort_index()
    
    data_entsoe['capacity_actual'] = data_entsoe.power_actual.rolling(20*24, center=True, min_periods=1).max()
    data_entsoe['cf_actual'] = data_entsoe['power_actual'] / data_entsoe['capacity_actual']
    data_entsoe.dropna(how='any', inplace=True)
         
    return data_entsoe

def load_countryinfo(trace_config):
    
    fname = f'{thewindpower_dir}countries/{trace_config["country"]}/wind-farm-data.csv'
    data_farms = pd.read_csv(fname)
    data_farms = data_farms.set_index('Name').filter(trace_config['farms'], axis=0)
    
    for name in trace_config['farms']:
        if name in corrections:
            for k,v in corrections[name].items():
                data_farms.loc[name,k] = v
    
    return data_farms

    
def load_era5(trace_config, data_farms, date_start, date_end, level):
    
    time_vect = np.arange(date_start, date_end, timedelta(hours=1))
    data_era5 = pd.DataFrame.from_dict({'DateTime': time_vect}).set_index('DateTime')
    
    for name in trace_config['farms']:
        
        farm = data_farms.loc[name]
    
        lat, lon = farm.Latitude, farm.Longitude
        
        #wind_u = get_era5_data(date_start, date_end, level, 'u', lat, lon, interpolated=True)
        #wind_v = get_era5_data(date_start, date_end, level, 'v', lat, lon, interpolated=True)
        #wind = (wind_u**2 + wind_v**2)**0.5
        
        wind = wind_speed_at_hub_height(date_start, date_end, lat, lon, farm['Hub height (m)'])
        
        data_era5[f'wind_{name}'] = wind
        
    return data_era5

def load_ninja(trace_config, date_start, date_end):
    if len(trace_config['farms']) != 1:
        raise NotImplementedError('[!] load_ninja() can only handle single farms')
        
    name = trace_config['farms'][0]
    fname = ninja_dir + f'data/{name}.csv' 
    data = pd.read_csv(fname, names=['DateTime', 'cf_ninja'], 
                header=1, index_col=0, parse_dates=True)
           
    data = data[(data.index >= date_start) & (data.index < date_end)]
    if len(data) != (date_end - date_start)//timedelta(hours=1):
        raise RuntimeError('[!] not enough ninja data!')
        
    return data

def load_thewindpower(trace_config, data_farms, date_start, date_end):
    
    data_curves = {}
    
    time_vect = np.arange(date_start, date_end, timedelta(hours=1))
    data_capacity = pd.DataFrame.from_dict({'DateTime': time_vect}).set_index('DateTime')
    
    for name in trace_config['farms']:
        
        farm = data_farms.loc[name]
        
        if np.isnan(farm['Hub height (m)']):
            raise Exception(f'[!] no hub height defined for {name}')
        
        curve_x, curve_y = load_power_curve(farm['Turbine model'])
        curve_y = curve_y / curve_y.max() # normalised
            
        if trace_config['type'] == 'country':
            date_start = parse_commission_date(farm.get('Commissioning')) or date_start
            date_end = parse_commission_date(farm.get('Decommissioning')) or date_end
            capacity = (data_capacity.index >= date_start) & (data_capacity.index < date_end)
        else:
            # assume all listed wind farms are operational for whole trace duration
            capacity = np.ones(len(data_capacity), dtype='float')
        capacity = capacity * farm['Power (kW)'] / 1000 # MW
        
        data_capacity[f'capacity_{name}'] = capacity
        data_curves[name] = curve_x, curve_y
        
    return data_capacity, data_curves

def load_all(trace_config, traces_dirs, era5_date_start, era5_date_end, level):
    data_entsoe = load_entsoe(trace_config, traces_dirs)
    
    data_entsoe = data_entsoe[(data_entsoe.index >= era5_date_start) & (data_entsoe.index < era5_date_end)]
    date_start = data_entsoe.index[0]
    date_end = data_entsoe.index[-1] + timedelta(hours=1)
    
    data_farms = load_countryinfo(trace_config)
    
    data_capacity, data_curves = load_thewindpower(trace_config, data_farms, date_start, date_end)
    data_era5 = load_era5(trace_config, data_farms, date_start, date_end, level)
    data_ninja = load_ninja(trace_config, date_start, date_end)
    
    data_time = data_entsoe.join([data_era5, data_capacity, data_ninja])
    
    return data_time, data_curves, data_farms