#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 12:06:26 2020

@author: liam
"""

import os
import numpy as np, pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from modules.access_era5 import get_era5_data

power_dir = '../data/power-10yrs/'
wind_gen_path = '../rawdata/wind-generators.csv'
power_curves_path = '../rawdata/power-curves.csv'

date_start = datetime(2010, 1, 1)
date_end = datetime(2020, 1, 1)

#sigma = 3.0 # (m/s) sigma of gaussian convolution to smooth the power curve
# to make it simulate lots of turbines with slightly different wind speeds
turbine = 'Vestas V112-Onshore'

#############
### funcs ###
#############

def remove_value(*arrays, value=np.nan):
    arrays = list(arrays)
    assert(arrays[0].ndim == 1)
    indices = set(range(len(arrays[0])))
    for array in arrays:
        assert(array.shape == arrays[0].shape)
        if np.isnan(value):
            temp = ~np.isnan(array)
        else:
            temp = array != value
        indices.intersection_update(np.where(temp)[0])
    for i in range(len(arrays)):
        arrays[i] = np.take(arrays[i], list(indices))
    return arrays

def calc_stats(x, y):
    c = np.corrcoef(x, y)[0,1] # correlation
    e = sum((y-x)**2) / len(x) # mean squared error
    return c, e
    
def chunkify(array, chunk_size):
    if chunk_size == 1:
        return array
    N = len(array) // chunk_size
    result = np.zeros(N)
    for i in range(N):
        result[i] = np.nanmean(array[i*chunk_size : (i+1)*chunk_size])
    return result

############
### main ###
############

farms = pd.read_csv(wind_gen_path)
farms = farms.sort_values('DUID')

power_curves = pd.read_csv(power_curves_path)
power_curves = power_curves.set_index('Wind Speed (m/s)')

results = []
sigs = []

for _, farm in farms.iterrows():
    uid = farm.DUID
    nameplate = farm['Max Cap (MW)']
    
    if uid != 'MACARTH1':
        continue
    
    power_fname = power_dir + uid + '.npy'
    if not os.path.exists(power_fname):
        print('[*] {:10} power data doesn\'t exist ({})'.format(uid, power_fname))
        continue
    
    # load up ERA5 wind and output power data, remove point where power = nan
    power = np.load(power_fname) / nameplate
    u_wind = get_era5_data(date_start, date_end, farm.Lat, farm.Lon, 'u', interpolated=True)
    v_wind = get_era5_data(date_start, date_end, farm.Lat, farm.Lon, 'v', interpolated=True)
    wind = (u_wind**2 + v_wind**2)**0.5
    
    #wind = chunkify(wind, 1)
    #power = chunkify(power, 1)
    
    #direction = 'ALL'
    #indices = select_by_direction(u_wind, v_wind, direction)
    #wind = np.take(wind, indices)
    #power = np.take(power, indices)
    
    wind, power = remove_value(wind, power)
    
    k, sigma, power_model = fit_power_curve(wind, power, turbine)
    c, e = calc_stats(power_model, power)
    p1,p0 = np.polyfit(power_model, power, 1)
    
    sigs.append(sigma)
    
    fig = plt.figure(figsize=(15,10))
    plt.xlabel('wind speed (m/s)')
    plt.ylabel('Capacity factor (measured)')
    plt.title(('{} ({})\n' + \
               'k={:.2f} sigma={:.2f}\n' + \
               'R={:.2f}, MSE={:.4f}\n' + \
               'y = {:.2f}x + {:.2f}').format(
               farm['Station Name'], uid, 1.1, 0, c, e, p1, p0))
    plt.scatter(wind, power, s=2, alpha=0.2)
    #plt.plot([0,power_model.max()], [p0,p0+p1*power_model.max()], color='black', linewidth=1)
    plt.show()
    #fig.savefig('../output/temp/' + uid + '.png', bbox_inches='tight')
    #plt.close()
    
    results.append({
            'DUID': uid,
            'name': farm['Station Name'],
            'R (correlation)': c,
            'MSE (mean sqr error)': e,
            'k (power curve scaling)': k,
            'sigma (power curve smoothing)': sigma,
            'p1 (linear fit gradient)': p1,
            'p0 (linear fit offset)': p0,
    })
    
results = pd.DataFrame(results)
results.to_csv('./temp.csv', index=False)
