#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 11:24:15 2020

@author: liam
"""

import pyproj, math, numpy as np


from wind.drives import era5_dir

n320_dir = era5_dir + 'n320/'

# the n320 grid (reduced gaussian grid with 320 latitudes between a pole and equator) 
# is expressed as a flat 1D array in the following order:
# 90lat/0lon -> 90lat/360lon -> ... -90lat/0lon -> -90lat/360lon

geod = pyproj.Geod(ellps='WGS84')

# the 2 * 320 latitudes of the n320 reduced gaussian grid:
lats = np.load(n320_dir + 'latitudes.npy')
# the number of evenly spaced longitudes on each of those latitudes, starting at 0deg:
lons = np.load(n320_dir + 'pts_per_latitude.npy')

# useful arrays:
i_lons_end = np.cumsum(lons) # last index (exclusive) of the ith latitude
i_lons_start = np.zeros(lons.shape, dtype='int')
i_lons_start[1:] = i_lons_end[:-1] # first index (inclusive) of the ith latitude

# distance in m between two points
def dist(lat1, lon1, lat2, lon2):
    return geod.inv(lon1, lat1, lon2, lat2)[2]

# finds index of closest lat/long coord in the N320 reduced Gaussian grid
# see https://confluence.ecmwf.int/display/UDOC/N320
def latlon_to_n320(lat, lon):
    
    i_lat = np.argmin(np.abs(lats - lat)) # index into lats of closest latitude
    i_lon_start = i_lons_start[i_lat] # index into flat n320 array to get to start of said latitude
        
    num_divs = lons[i_lat] # split up 360 degs of longitude by number of divisions
    i_lon = int(round(lon / 360.0 * num_divs, 0)) % num_divs # index of closest one
   # print(num_divs, i_lon)
    
    return i_lon_start + i_lon


def n320_to_latlon(i_n320):
    
    i_lat = np.sum(i_n320 >= i_lons_end)
    
    lat = lats[i_lat]
    i_lon_start = i_lons_start[i_lat]
    
    i_lon = i_n320 - i_lon_start
    num_divs = lons[i_lat]
    lon = (i_lon * 360.0 / num_divs) % 360
    
    return lat, lon


# find the four closest n320 grid points in the NW, NE, SW, SE directions
# assign each a weight to use for interpolation (inverse distance to original point)
# this is not proper bilinear quadrilateral interpolation, but it's pretty good
def latlon_to_n320_interpolated(lat, lon):
    
    # bound lat within range
    if lat > lats[0]:
        lat = lats[0]
    elif lat < lats[-1]:
        lat = lats[-1]
    
    i_lat1, i_lat2 = np.argpartition(np.abs(lats - lat), 2)[:2] # get the two closest lats
    
    lat1 = lats[i_lat1] # closest lat
    i_lon_start1 = i_lons_start[i_lat1]
    
    lat2 = lats[i_lat2] # second closest lat
    i_lon_start2 = i_lons_start[i_lat2]
    
    num_divs1 = lons[i_lat1]
    i_lon1A = math.floor(lon / 360.0 * num_divs1) % num_divs1
    i_lon1B = math.ceil(lon / 360.0 * num_divs1) % num_divs1
    lon1A = i_lon1A * 360.0 / num_divs1
    lon1B = i_lon1B * 360.0 / num_divs1
    
    num_divs2 = lons[i_lat2]
    i_lon2A = math.floor(lon / 360.0 * num_divs2) % num_divs2
    i_lon2B = math.ceil(lon / 360.0 * num_divs2) % num_divs2
    lon2A = i_lon2A * 360.0 / num_divs2
    lon2B = i_lon2B * 360.0 / num_divs2
    
    results = {
            i_lon_start1 + i_lon1A: 1/dist(lat, lon, lat1, lon1A),
            i_lon_start1 + i_lon1B: 1/dist(lat, lon, lat1, lon1B),
            i_lon_start2 + i_lon2A: 1/dist(lat, lon, lat2, lon2A),
            i_lon_start2 + i_lon2B: 1/dist(lat, lon, lat2, lon2B),
    }
    scale = sum(results.values())
    results = {k: v/scale for k,v in results.items()}
    
    return results

