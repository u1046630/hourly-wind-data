
import numpy as np
from scipy import optimize

from wind.power_curves import get_power_curve
    
def fit_power_curve(wind, power, turbine):
   
    def func(x, k, sigma, P0):
        xp, fp = get_power_curve(turbine, k, sigma, P0)
        return np.interp(wind, xp, fp)
    
    params,_ = optimize.curve_fit(func, wind, power, p0=(1.0, 1.5, 1.0))
    k,sigma, P0 = params
    return k, sigma, P0, func(wind, k, sigma, P0)

def calc_stats(x, y):
    c = np.corrcoef(x, y)[0,1] # correlation
    e = sum((y-x)**2) / len(x) # mean squared error
    return c, e

def relative_density(T):
    # T: temperature (K)
    # result: air density relative to air at 15 celcius
    # m calculated from linear regression of values found at https://en.wikipedia.org/wiki/Density_of_air
    m = 288.057
    x0 = 1/(273.15 + 15)
    x = 1/T
    return m*(x - x0) + 1.0
    

def select_wind_direction(u_wind, v_wind, direction):
    # u is +ve for west to east
    # v is +ve for north to south
    # angle = 0 when flowing east, 90 when flowing south, 180 when flowing west, 270 when flowing north
    angle = (np.degrees(np.arctan(v_wind/u_wind)) + 180.0 * (u_wind < 0.0)) % 360.0
    
    parts = 90/4
    if direction == 'W':
        selected = ((angle >= 0*parts) & (angle < 1*parts)) | ((angle >= 15*parts) & (angle < 16*parts))
    elif direction == 'NW':
        selected = (angle >= 1*parts) & (angle < 3*parts)
    elif direction == 'N':
        selected = (angle >= 3*parts) & (angle < 5*parts)
    elif direction == 'NE':
        selected = (angle >= 5*parts) & (angle < 7*parts)
    elif direction == 'E':
        selected = (angle >= 7*parts) & (angle < 9*parts)
    elif direction == 'SE':
        selected = (angle >= 9*parts) & (angle < 11*parts)
    elif direction == 'S':
        selected = (angle >= 11*parts) & (angle < 13*parts)
    elif direction == 'SW':
        selected = (angle >= 13*parts) & (angle < 15*parts)
    elif direction == 'ALL':
        selected = (angle == angle)
    else:
        print('[!] bad wind direction ({})'.format(direction))
        raise(Warning)
        
    return np.where(selected)[0]

def __line_to_nums(line, dtype='float'):
    return np.array([x for x in line.split(' ') if x != ''], dtype=dtype)

def read_gwc_file(f):
    lines = f.readlines()
    
    line = lines.pop(0)
    assert(line.startswith('Global Wind Atlas 3.0 (WRF 3-km)'))
    lat,lon,_ = line.split('<coordinates>')[-1].split('</coordinates>')[0].split(',')
    lat, lon = np.float(lat), np.float(lon)
    
    num_roughness, num_heights, num_sectors = __line_to_nums(lines.pop(0), dtype='int')
    
    roughness = __line_to_nums(lines.pop(0))
    heights = __line_to_nums(lines.pop(0))
    
    freq = np.full((num_roughness, num_sectors), np.nan)
    a_param = np.full((num_roughness, num_heights, num_sectors), np.nan)
    k_param = np.full((num_roughness, num_heights, num_sectors), np.nan)
    
    
    for i in range(num_roughness):
        freq[i:] = __line_to_nums(lines.pop(0))
        for j in range(num_heights):
            a_param[i,j,:] = __line_to_nums(lines.pop(0))
            k_param[i,j,:] = __line_to_nums(lines.pop(0))
            
    assert(len(lines) == 0)
    return roughness, heights, freq, a_param, k_param
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    