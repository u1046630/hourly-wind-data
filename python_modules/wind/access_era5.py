#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 12:15:02 2020

@author: liam
"""

import numpy as np
from datetime import datetime, timedelta

import wind.n320_grid as n320_grid
from wind.drives import era5_dir

# important information relating to how I've stored the ERA5 data
num_n320_pts = 542080
num_n320_pts_per_file = 1120

def year(x):
    return datetime(x, 1, 1)

# maps path to date_start, date_end, level, parameter_list
downloads = {
    'level131-1980-1989/data/': (year(1980), year(1990), 131, ('u','v')),
    'level131-1990-1999/data/': (year(1990), year(2000), 131, ('u','v')),
    'level131-2000-2009/data/': (year(2000), year(2010), 131, ('u','v')),
    'level131-2010-2019/data/': (year(2010), year(2020), 131, ('u','v')),
    
    'level133-2008-2009/data/': (year(2008), year(2010), 133, ('u','v')),
    'level133-2010-2019/data/': (year(2010), year(2020), 133, ('u','v','t')),
}

###################
### pre compute ###
###################

num_files = num_n320_pts // num_n320_pts_per_file

# assume same format for .npz naming in all downloads
fnames = []
for i in range(num_files):
    i_start = i * num_n320_pts_per_file # inclusive
    i_end = i_start + num_n320_pts_per_file -1 # inclusive
    fnames.append('{}_{}.npz'.format(i_start, i_end))

#####################
### get_era5_data ###
#####################
    
# from_date, to_date: datetime objects, inclusive and exclusive
# level: int, ERA5 model level
# param: str, 'u' for eastward wind, 'v' for northward wind, 't' for temp
# lat, lon: floats, WGS84
# interpolated: bool
    
def get_era5_data(date_from, date_to, level, param, lat, lon, interpolated=False):
    
    # date_from and to_date represent the requested period
    # date1 and date2 represent the period STORED in a particular download directory
    # dateA and dateB represent the period NEEDED from a particular download directory
    # all of them are inclusive, exclusive respectively
        
    periods = {} # similar format as downloads (without level and param)
    temp_date = date_from
    while temp_date < date_to:
        for path,(date1,date2,lvl,params) in downloads.items():
            if level == lvl and param in params:
                if temp_date >= date1 and temp_date < date2:
                    periods[path] = (temp_date, min(date2, date_to))
                    temp_date = date2
                    break
        else:
            raise Exception('[!] no data avaliable (date={}, param={})'.format(
                temp_date, param
            ))
        
    hour = timedelta(hours=1)
    data = np.zeros((date_to - date_from) // hour)
    
    for path,(dateA,dateB) in periods.items():
        date1 = downloads[path][0] # start of STORED downloaded
        
        nhrs = (dateB - dateA) // hour
        i_src = (dateA - date1) // hour
        i_dst = (dateA - date_from) // hour
        
        if interpolated:
            weights = n320_grid.latlon_to_n320_interpolated(lat, lon)
        else:
            weights = {n320_grid.latlon_to_n320(lat, lon): 1.0}
            
        for i_n320, weight in weights.items():
            i_file = i_n320 // num_n320_pts_per_file
            fname = era5_dir + path + fnames[i_file]
            array_name = '{}_{}'.format(i_n320, param.lower())
            array = np.load(fname)[array_name] * weight
            
            data[i_dst:(i_dst+nhrs)] += array[i_src:(i_src+nhrs)]
            
    return data