
import pandas as pd, numpy as np, os
from scipy import ndimage

from wind.drives import thewindpower_dir #windturbinemodels_dir

#power_curves_path = windturbinemodels_dir + 'power-curves.csv'
#power_curves = pd.read_csv(power_curves_path)
#power_curves = power_curves.set_index('Wind Speed (m/s)')

def get_power_curve(turbine, k=0, sigma=None):
    fname = f'{thewindpower_dir}turbines/power-curve-data/{turbine}.csv'
    if not os.path.exists(fname):
        raise Exception('[!] power curve for {} doesnt exist'.format(turbine))
    power_curve = pd.read_csv(fname)
    curve_x = np.array(power_curve['Wind speed (m/s)'])
    curve_y = np.array(power_curve['Power (kW)'])
    
    curve_x += k
    if sigma:
        step = curve_x[1] - curve_x[0]
        sigma /= step
        curve_y = ndimage.filters.gaussian_filter1d(curve_y, sigma, mode='nearest')
    return curve_x, curve_y

